﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.CurrentUserProviders;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Configuration
{
    public static class ApplicationServiceCollectionExtensions
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.AddSingleton<IDateTimeProvider, LocalDateTimeProvider>();
            services.AddTransient<ICurrentUserProvider, CurrentUserProvider>();
            return services;
        }

        public static IServiceCollection AddUnitOfWork(this IServiceCollection services, string connectionString)
        {
            services.AddSingleton<IUnitOfWorkProvider>(sp => new UnitOfWorkProvider(sp, sp.GetRequiredService<ILoggerFactory>(), connectionString));
            return services;
        }
    }
}
