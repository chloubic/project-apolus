﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Project_Apolus.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Project_Apolus.Configuration
{
    public static class AutoMapperServiceCollectionExtensions
    {
        public static void AddAutoMapper(this IServiceCollection services)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                var mappings = GetAllMappingsFromProject();
                mappings.ForEach(x => x.ConfigureMaps(cfg));
            });

            var mapper = mapperConfig.CreateMapper();
            mapper.ConfigurationProvider.AssertConfigurationIsValid();

            services.AddSingleton(mapper);
        }

        private static List<IMapping> GetAllMappingsFromProject()
        {
            return Assembly.GetAssembly(typeof(IMapping))!
                .GetTypes()
                .Where(x => typeof(IMapping).IsAssignableFrom(x)
                            && !x.IsInterface
                            && !x.IsAbstract)
                .Select(x => (IMapping)Activator.CreateInstance(x)!)
                .ToList();
        }
    }
}
