﻿using Microsoft.Extensions.DependencyInjection;
using Project_Apolus.BL;
using Project_Apolus.BL.EmailProviders;
using Project_Apolus.BL.Facades;
using Project_Apolus.BL.Logger;
using Project_Apolus.BL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Configuration
{
    public static class CommonServiceCollectionExtension
    {
        public static void AddAllRepositoriesAsTransient<TFromAssemblyOfType>(this IServiceCollection services)
        {
            services.Scan(scan =>
                scan.FromAssemblyOf<TFromAssemblyOfType>()
                    .AddClasses(cls => cls.AssignableTo(typeof(IRepository<,>)))
                    .AsSelfWithInterfaces()
                    .WithTransientLifetime()
            );
        }

        public static void AddAllFacadesAsTransient<TFromAssemblyOfType>(this IServiceCollection services)
        {
            services.Scan(scan =>
                scan.FromAssemblyOf<TFromAssemblyOfType>()
                    .AddClasses(cls => cls.AssignableTo<IFacade>())
                    .AsSelfWithInterfaces()
                    .WithTransientLifetime()
            );
        }

        public static void AddStorageAsTransient<TFromAssemblyOfType>(this IServiceCollection services)
        {
            services.Scan(scan =>
                scan.FromAssemblyOf<TFromAssemblyOfType>()
                .AddClasses(cls => cls.AssignableTo<IStorage>())
                .AsSelfWithInterfaces()
                .WithTransientLifetime()
            );
        }

        public static void AddEmailSenderAsTransient<TFromAssemblyOfType>(this IServiceCollection services)
        {
            services.Scan(scan =>
                scan.FromAssemblyOf<TFromAssemblyOfType>()
                .AddClasses(cls => cls.AssignableTo<IEmailClientSender>())
                .AsSelfWithInterfaces()
                .WithTransientLifetime()
            );
        }

        public static void AddLoggerAsTransient<TFromAssemblyOfType>(this IServiceCollection services)
        {
            services.Scan(scan =>
                scan.FromAssemblyOf<TFromAssemblyOfType>()
                .AddClasses(cls => cls.AssignableTo<ILogger>())
                .AsSelfWithInterfaces()
                .WithTransientLifetime()
            );
        }
    }
}
