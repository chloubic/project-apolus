﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Project_Apolus.BL.Facades;
using Project_Apolus.BL.Logger;
using Project_Apolus.Common.Model.Auth;
using Project_Apolus.CurrentUserProviders;
using Project_Apolus.DAL.Entities.Identity;
using Project_Apolus.Settings;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthenticationFacade authenticationFacade;
        private readonly ILogger logger;
        private readonly UserManager<AppUser> userManager;
        private readonly SignInManager<AppUser> signInManager;
        private readonly SigningCredentialsConfiguration signingCredentialsConfiguration;
        private readonly ICurrentUserProvider currentUserProvider;

        public AuthController(IAuthenticationFacade authenticationFacade, ILogger logger,
            UserManager<AppUser> userManager, SignInManager<AppUser> signInManager,
            SigningCredentialsConfiguration signingCredentialsConfiguration,
            ICurrentUserProvider currentUserProvider)
        {
            this.authenticationFacade = authenticationFacade;
            this.logger = logger;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.signingCredentialsConfiguration = signingCredentialsConfiguration;
            this.currentUserProvider = currentUserProvider;
        }

        [HttpPost]
        public async Task<ActionResult<LoginResultDto>> Login([FromBody] LoginDto loginDto)
        {
            if (!ModelState.IsValid) return BadRequest();

            var user = await userManager.FindByEmailAsync(loginDto.Email);
            if (user == null) return NotFound();

            var result =
                await signInManager.CheckPasswordSignInAsync(user, loginDto.Password, lockoutOnFailure: false);

            if (!result.Succeeded)
            {
                return Unauthorized();
            }

            //switch (user.PasswordState)
            //{
            //    case PasswordState.RegularPassword:
            //        break;
            //    case PasswordState.OneTimePasswordActive:
            //        await authenticationFacade.MarkOneTimePasswordUsedAsync(user);
            //        loginResult.IsOneTimePassword = true;
            //        break;
            //    case PasswordState.OneTimePasswordUsed:
            //        return Unauthorized();
            //    default:
            //        throw new ArgumentOutOfRangeException(nameof(user.PasswordState));
            //}

            var claims = await userManager.GetClaimsAsync(user);

            var secret = signingCredentialsConfiguration.JwtKey;

            var token = new JwtSecurityToken
            (
                issuer: "Apolus",
                audience: "auidience",
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(60),
                notBefore: DateTime.UtcNow,
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey
                        (Encoding.UTF8.GetBytes(secret)),
                    SecurityAlgorithms.HmacSha256)
            );
            logger.Log($"[{DateTime.Now.ToString()}] - Uživatel přihlášen: {user.Email}");
            //var lastLogin = await logFacade.GetLastLoginByUserAsync(user.Id);
            //token.Payload["lastLogin"] = lastLogin;
            //await logFacade.CreateAsync(user.Id);
            var loginResult = new LoginResultDto(new JwtSecurityTokenHandler().WriteToken(token));
            return loginResult;
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDto dto)
        {
            if (!ModelState.IsValid) return BadRequest();

            var loggedUser = currentUserProvider.GetRequiredLoggedUser();

            await authenticationFacade.ChangePasswordAsync(dto, loggedUser.Id);
            logger.Log($"[{DateTime.Now.ToString()}] - Změna hesla uživatele: {loggedUser.Email}");
            return Ok();
        }
    }
}
