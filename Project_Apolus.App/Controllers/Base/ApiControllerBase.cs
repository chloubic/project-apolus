﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Project_Apolus.BL.Logger;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Controllers.Base
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ApiControllerBase : ControllerBase
    {
        protected readonly IUnitOfWorkProvider unitOfWorkProvider;
        protected readonly ILogger logger;

        protected ApiControllerBase(IUnitOfWorkProvider unitOfWorkProvider, ILogger logger)
        {
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.logger = logger;
        }
    }
}
