﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project_Apolus.BL.Facades;
using Project_Apolus.BL.Logger;
using Project_Apolus.Controllers.Base;
using Project_Apolus.CurrentUserProviders;
using Project_Apolus.DAL.Infrastructure;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Controllers
{
    public class ImageController : ApiControllerBase
    {
        private readonly IImageFacade imageFacade;
        private readonly ICurrentUserProvider currentUserProvider;
        public ImageController(IUnitOfWorkProvider unitOfWorkProvider, IImageFacade imageFacade, ILogger logger, ICurrentUserProvider currentUserProvider) : base(unitOfWorkProvider, logger)
        {
            this.imageFacade = imageFacade;
            this.currentUserProvider = currentUserProvider;
        }

        [HttpPost]
        [SwaggerResponse(201)]
        public async Task<IActionResult> UploadImage(IFormFile file)
        {
            var fileName = await imageFacade.UploadImage(file);
            //logger.Log($"[{DateTime.Now.ToString()}] - Nahrán obrázek uživatelem {currentUserProvider.GetRequiredLoggedUser().Email}");
            return CreatedAtAction(nameof(GetImage), new { fileName }, null);
        }

        [AllowAnonymous]
        [HttpGet("{fileName}")]
        public async Task<IActionResult> GetImage(string fileName)
        {
            var image = await imageFacade.GetImageAsync(fileName);
            if (image == null)
            {
                return NotFound();
            }
            var extension = Path.GetExtension(fileName).Replace(".", "");
            if (extension.Equals("svg")) extension = "svg+xml";
            return File(image, $"image/{extension}");
        }
    }
}
