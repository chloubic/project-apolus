﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Project_Apolus.BL.Facades;
using Project_Apolus.BL.Logger;
using Project_Apolus.Common.Enums;
using Project_Apolus.Common.Model.Menu;
using Project_Apolus.Controllers.Base;
using Project_Apolus.CurrentUserProviders;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.Controllers
{
    public class MenuController : ApiControllerBase
    {

        private readonly IMenuFacade MenuFacade;

        private readonly ICurrentUserProvider CurrentUserProvider;

        public MenuController(IUnitOfWorkProvider unitOfWorkProvider, ILogger logger, IMenuFacade menuFacade, ICurrentUserProvider currentUserProvider) : base(unitOfWorkProvider, logger)
        {
            MenuFacade = menuFacade;
            CurrentUserProvider = currentUserProvider;
        }

        [AllowAnonymous]
        [HttpGet("Categories/{restaurantId}")]
        public async Task<IList<MenuCategoryDto>> GetMenuCategories(CancellationToken ct, Guid restaurantId)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await MenuFacade.GetMenuCategories(uow, ct, restaurantId);
            }
        }

        [AllowAnonymous]
        [HttpGet("Category/{id}")]
        public async Task<IList<MenuItemDto>> GetCategoryItems(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await MenuFacade.GetCategoryItems(uow, ct, id);
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpDelete("Category/{id}")]
        public async Task<IActionResult> DeleteCategory(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await MenuFacade.DeleteMenuCategory(uow, ct, id, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpPost("Categories/{restaurantId}")]
        public async Task<IActionResult> CreateMenuCategory(CancellationToken ct, Guid restaurantId, [FromBody] CreateMenuDto categories)
        {
            if (!ModelState.IsValid) return BadRequest();
            using (var uow = unitOfWorkProvider.Create())
            {
                await MenuFacade.CreateMenuCategory(uow, ct, restaurantId, categories, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpPost("Items/{menuId}")]
        public async Task<IActionResult> CreateMenuItems(CancellationToken ct, Guid menuId, [FromBody] CreateMenuItemDto items)
        {
            if (!ModelState.IsValid) return BadRequest();
            using (var uow = unitOfWorkProvider.Create())
            {
                await MenuFacade.CreateMenuItems(uow, ct, menuId, items, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }

        // TODO: Not working
        [Authorize(Roles = Roles.Restaurant)]
        [HttpPut("Categories")]
        public async Task<IActionResult> UpdateMenuCategory(CancellationToken ct,[FromBody] UpdateMenuDto categories)
        {
            if (!ModelState.IsValid) return BadRequest();
            using (var uow = unitOfWorkProvider.Create())
            {
                await MenuFacade.UpdateMenuCategory(uow, ct, categories, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }

        // TODO: Not working
        [Authorize(Roles = Roles.Restaurant)]
        [HttpPut("Items")]
        public async Task<IActionResult> UpdateMenuItems(CancellationToken ct, [FromBody] UpdateMenuItemDto items)
        {
            if (!ModelState.IsValid) return BadRequest();
            using (var uow = unitOfWorkProvider.Create())
            {
                await MenuFacade.UpdateMenuItem(uow, ct, items, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpPut("Category/Order/{id}")]
        public async Task<IActionResult> ChangeCategoryOrder(CancellationToken ct, Guid id, [FromBody] IList<ItemOrderUpdateDto> items)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await MenuFacade.ChangeMenuCategoryOrder(uow, ct, id, items, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }


        [Authorize(Roles = Roles.Restaurant)]
        [HttpPut("Items/Order/{id}")]
        public async Task<IActionResult> ChangeItemsOrder(CancellationToken ct, Guid id, [FromBody] IList<ItemOrderUpdateDto> items)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await MenuFacade.ChangeMenuItemsOrder(uow, ct, id, items, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpDelete("Item/{id}")]
        public async Task<IActionResult> DeleteMenuItem(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await MenuFacade.DeleteMenuItem(uow, ct, id, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpGet("Category/Info/{id}")]
        public async Task<MenuCategoryDto> GetCategoryDetail(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await MenuFacade.GetCategoryInfoAsync(uow, ct, id);
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpGet("Item/Info/{id}")]
        public async Task<MenuItemDto> GetItemDetail(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await MenuFacade.GetItemInfoAsync(uow, ct, id);
            }
        }

    }
}
