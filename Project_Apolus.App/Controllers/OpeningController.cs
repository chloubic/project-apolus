﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Project_Apolus.BL.Facades;
using Project_Apolus.BL.Logger;
using Project_Apolus.Common.Enums;
using Project_Apolus.Common.Model.Opening;
using Project_Apolus.Controllers.Base;
using Project_Apolus.CurrentUserProviders;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.Controllers
{
    public class OpeningController : ApiControllerBase
    {

        private readonly IOpeningFacade OpeningFacade;
        private readonly ICurrentUserProvider CurrentUserProvider;

        public OpeningController(IUnitOfWorkProvider unitOfWorkProvider, ILogger logger, ICurrentUserProvider currentUserProvider,
            IOpeningFacade openingFacade) : base(unitOfWorkProvider, logger)
        {
            OpeningFacade = openingFacade;
            CurrentUserProvider = currentUserProvider;
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpPost("Update/{id}")]
        public async Task<IActionResult> UpdateRestaurantOpeningHours(CancellationToken ct, Guid id, [FromBody] IList<OpeningDto> openingDtos)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await OpeningFacade.UpdateRestaurantOpeningHours(openingDtos, id, CurrentUserProvider.GetRequiredLoggedUser().Id, uow, ct);
                return Ok();
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpGet("{id}")]
        public async Task<IList<OpeningDto>> GetRestaurantOpeningHours(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await OpeningFacade.GetRestaurantOpeningHours(id, CurrentUserProvider.GetRequiredLoggedUser().Id, uow, ct);
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpGet("Days")]
        public async Task<IList<DayDto>> GetDays(CancellationToken ct)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await OpeningFacade.GetDays(uow, ct);
            }
        }
    }
}
