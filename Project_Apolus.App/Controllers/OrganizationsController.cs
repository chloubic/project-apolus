﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Project_Apolus.BL.Facades;
using Project_Apolus.BL.Logger;
using Project_Apolus.Common.Enums;
using Project_Apolus.Common.Model.Organization;
using Project_Apolus.Controllers.Base;
using Project_Apolus.CurrentUserProviders;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.Controllers
{
    public class OrganizationsController : ApiControllerBase
    {

        private readonly IOrganizationFacade OrganizationFacade;
        private readonly ICurrentUserProvider CurrentUserProvider;

        public OrganizationsController(IUnitOfWorkProvider unitOfWorkProvider, ILogger logger, IOrganizationFacade organizationFacade,
            ICurrentUserProvider currentUserProvider) : base(unitOfWorkProvider, logger)
        {
            OrganizationFacade = organizationFacade;
            CurrentUserProvider = currentUserProvider;
        }

        [Authorize(Roles = Roles.Admin)]
        [HttpGet]
        public async Task<OrganizationDto> GetOrganization(CancellationToken ct)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await OrganizationFacade.GetOrganizationAsync(CurrentUserProvider.GetRequiredLoggedUser().Id, uow, ct);
            }
        }

        [Authorize(Roles = Roles.Admin)]
        [HttpPut("Update")]
        public async Task<IActionResult> UpdateOrganization(CancellationToken ct, [FromBody] UpdateOrganizationDto updateDto)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await OrganizationFacade.UpdateOrganizationAsync(updateDto, CurrentUserProvider.GetRequiredLoggedUser().Id, uow, ct);
                return Ok();
            }
        }
    }
}
