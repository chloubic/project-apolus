﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Project_Apolus.BL.Facades;
using Project_Apolus.BL.Logger;
using Project_Apolus.Common.Enums;
using Project_Apolus.Common.Model.Photogallery;
using Project_Apolus.Controllers.Base;
using Project_Apolus.CurrentUserProviders;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.Controllers
{
    public class PhotogalleryController : ApiControllerBase
    {

        private readonly IPhotogalleryFacade PhotogalleryFacade;
        private readonly ICurrentUserProvider CurrentUserProvider;

        public PhotogalleryController(IUnitOfWorkProvider unitOfWorkProvider, ILogger logger, IPhotogalleryFacade photogalleryFacade,
            ICurrentUserProvider currentUserProvider) : base(unitOfWorkProvider, logger)
        {
            PhotogalleryFacade = photogalleryFacade;
            CurrentUserProvider = currentUserProvider;
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpPost("Update/{id}")]
        public async Task<IActionResult> UpdateRestaurantPhotos(CancellationToken ct, Guid id, [FromBody] PhotosDto photosDto)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await PhotogalleryFacade.UpdateRestaurantPhotos(photosDto, id, CurrentUserProvider.GetRequiredLoggedUser().Id, uow, ct);
                return Ok();
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IList<string>> GetRestaurantPhotos(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await PhotogalleryFacade.GetRestaurantPhotos(id, uow, ct);
            }
        }
    }
}
