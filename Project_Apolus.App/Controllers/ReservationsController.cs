﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Project_Apolus.BL.Facades;
using Project_Apolus.BL.Logger;
using Project_Apolus.Common.Enums;
using Project_Apolus.Common.Model.Reservations;
using Project_Apolus.Controllers.Base;
using Project_Apolus.CurrentUserProviders;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.Controllers
{
    public class ReservationsController : ApiControllerBase
    {

        private readonly IReservationsFacade ReservationsFacade;
        private readonly ICurrentUserProvider CurrentUserProvider;

        public ReservationsController(IUnitOfWorkProvider unitOfWorkProvider, ILogger logger, IReservationsFacade reservationsFacade,
            ICurrentUserProvider currentUserProvider) : base(unitOfWorkProvider, logger)
        {
            ReservationsFacade = reservationsFacade;
            CurrentUserProvider = currentUserProvider;
        }

        [Authorize(Roles = Roles.Client)]
        [HttpGet("Client")]
        public async Task<IList<ReservationDto>> GetMyReservations(CancellationToken ct)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await ReservationsFacade.GetMyReservationsAsync(uow, ct, CurrentUserProvider.GetRequiredLoggedUser().Id);
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpGet("Restaurant/{id}")]
        public async Task<IList<ReservationDto>> GetRestaurantReservations(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await ReservationsFacade.GetRestaurantReservationsAsync(uow, ct, id, CurrentUserProvider.GetRequiredLoggedUser().Id);
            }
        }

        [Authorize(Roles = Roles.Client)]
        [HttpPost("{id}")]
        public async Task<IActionResult> CreateReservation(CancellationToken ct, Guid id, [FromBody] CreateReservationDto reservationDto)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await ReservationsFacade.CreateReservationAsync(uow, ct, id, reservationDto, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }

        [Authorize(Roles = Roles.Client)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReservation(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await ReservationsFacade.DeleteReservationAsync(uow, ct, id, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }

    }
}
