﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Project_Apolus.BL.Facades;
using Project_Apolus.BL.Logger;
using Project_Apolus.Common.Enums;
using Project_Apolus.Common.Filters;
using Project_Apolus.Common.Model.Restaurant;
using Project_Apolus.Controllers.Base;
using Project_Apolus.CurrentUserProviders;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.Controllers
{
    public class RestaurantsController : ApiControllerBase
    {

        private readonly IRestaurantFacade RestaurantFacade;
        private readonly ICurrentUserProvider CurrentUserProvider;

        public RestaurantsController(IUnitOfWorkProvider unitOfWorkProvider, ILogger logger, IRestaurantFacade restaurantFacade,
            ICurrentUserProvider currentUserProvider) : base(unitOfWorkProvider, logger)
        {
            RestaurantFacade = restaurantFacade;
            CurrentUserProvider = currentUserProvider;
        }

        [AllowAnonymous]
        [HttpGet("Detail/{id}")]
        public async Task<RestaurantDetailDto> GetRestaurantDetail(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await RestaurantFacade.GetRestaurantDetail(id, uow, ct);
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpGet("ByOrganization/{id}")]
        public async Task<IList<RestaurantDto>> GetRestaurantsByOrganization(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await RestaurantFacade.GetRestaurantsByOrganization(id, uow, ct);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<RestaurantResultDto> GetRestaurants(CancellationToken ct, string? name = null!, int page = 1, int limit = 200, bool descOrder = true)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                var filter = new RestaurantFilter(name ?? null!, page, limit, descOrder);
                return new RestaurantResultDto()
                {
                    Data = await RestaurantFacade.GetFilteredRestaurantsAsync(filter, uow, ct),
                    Total = await RestaurantFacade.GetFilteredRestaurantsCountAsync(filter, uow, ct)
                };
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpPost("Create")]
        public async Task<IActionResult> CreateRestaurant(CancellationToken ct, [FromBody] CreateRestaurantDto createRestaurantDto)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await RestaurantFacade.CreateRestaurantAsync(createRestaurantDto, CurrentUserProvider.GetRequiredLoggedUser().Id, uow, ct);
                return Ok();
            }
        }


        [Authorize(Roles = Roles.Restaurant)]
        [HttpPut("Update")]
        public async Task<IActionResult> UpdateRestaurant(CancellationToken ct, [FromBody] UpdateRestaurantDto updateRestaurantDto)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await RestaurantFacade.UpdateRestaurantAsync(updateRestaurantDto, CurrentUserProvider.GetRequiredLoggedUser().Id, uow, ct);
                return Ok();
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> DeleteRestaurant(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await RestaurantFacade.DeleteRestaurantAsync(id, CurrentUserProvider.GetRequiredLoggedUser().Id, uow, ct);
                return Ok();
            }
        }

    }
}
