﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Project_Apolus.BL.Facades;
using Project_Apolus.BL.Logger;
using Project_Apolus.Common.Enums;
using Project_Apolus.Common.Model.Tables;
using Project_Apolus.Common.Params;
using Project_Apolus.Controllers.Base;
using Project_Apolus.CurrentUserProviders;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.Controllers
{
    public class TableController : ApiControllerBase
    {

        private readonly ITablesFacade TablesFacade;

        private readonly ICurrentUserProvider CurrentUserProvider;

        public TableController(IUnitOfWorkProvider unitOfWorkProvider, ILogger logger, ITablesFacade tablesFacade, ICurrentUserProvider currentUserProvider) : base(unitOfWorkProvider, logger)
        {
            TablesFacade = tablesFacade;
            CurrentUserProvider = currentUserProvider;
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpGet("All/{id}")]
        public async Task<IList<TableDto>> GetTables(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await TablesFacade.GetAllTablesAsync(uow, ct, id, CurrentUserProvider.GetRequiredLoggedUser().Id);
            }
        }

        [AllowAnonymous]
        [HttpGet("Available/{id}")]
        public async Task<ActionResult<IList<TableDto>>> GetAvailableTables(CancellationToken ct, Guid id, [FromQuery] TablesQuery param)
        {
            if (!ModelState.IsValid) return BadRequest();
            using (var uow = unitOfWorkProvider.Create())
            {
                return Ok(await TablesFacade.GetAvailableTablesAsync(uow, ct, id, param));
            }
        }


        [Authorize(Roles = Roles.Restaurant)]
        [HttpPost("{id}")]
        public async Task<IActionResult> CreateTable(CancellationToken ct, Guid id, [FromBody] CreateTableDto table)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await TablesFacade.CreateTableAsync(uow, ct, id, table, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }

        [Authorize(Roles = Roles.Restaurant)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTable(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await TablesFacade.DeleteTableAsync(uow, ct, id, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }

    }
}
