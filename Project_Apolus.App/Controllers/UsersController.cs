﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Project_Apolus.BL.Facades;
using Project_Apolus.BL.Logger;
using Project_Apolus.Common.Enums;
using Project_Apolus.Common.Model.User;
using Project_Apolus.Controllers.Base;
using Project_Apolus.CurrentUserProviders;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.Controllers
{
    public class UsersController : ApiControllerBase
    {

        private readonly IUserFacade UserFacade;
        private readonly ICurrentUserProvider CurrentUserProvider;

        public UsersController(IUnitOfWorkProvider unitOfWorkProvider, ILogger logger, IUserFacade userFacade, ICurrentUserProvider currentUserProvider) : base(unitOfWorkProvider, logger)
        {
            UserFacade = userFacade;
            CurrentUserProvider = currentUserProvider;
        }

        [AllowAnonymous]
        [HttpPost("CreateAdmin")]
        public async Task<IActionResult> CreateAdmin(CancellationToken ct, [FromBody] CreateAdminDto createAdminDto)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await UserFacade.CreateAdmin(createAdminDto, uow, ct);
                return Ok();
            }
        }

        [Authorize(Roles = Roles.Admin)]
        [HttpPost("CreateOperator")]
        public async Task<IActionResult> CreateOperator(CancellationToken ct, [FromBody] CreateClientDto createOperatorDto)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await UserFacade.CreateOperator(createOperatorDto, uow, ct, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }

        [AllowAnonymous]
        [HttpPost("CreateClient")]
        public async Task<IActionResult> CreateClient(CancellationToken ct, [FromBody] CreateClientDto createClientDto)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await UserFacade.CreateClient(createClientDto, uow, ct);
                return Ok();
            }
        }

        [Authorize(Roles = Roles.Admin)]
        [HttpGet("Operators")]
        public async Task<IList<OperatorDto>> GetOperators(CancellationToken ct)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await UserFacade.GetOperatorsAsync(uow, ct, CurrentUserProvider.GetRequiredLoggedUser().Id);
            }
        }

        [HttpGet("Info")]
        public async Task<UserDto> GetUserInfo(CancellationToken ct)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                return await UserFacade.GetUserInfo(uow, ct, CurrentUserProvider.GetRequiredLoggedUser().Id);
            }
        }

        [HttpPut("Info")]
        public async Task<IActionResult> UpdateUserInfo(CancellationToken ct, [FromBody] UserDto user)
        {
            if (!ModelState.IsValid) return BadRequest();
            using (var uow = unitOfWorkProvider.Create())
            {
                await UserFacade.UpdateUserInfoAsync(uow, ct, user, CurrentUserProvider.GetRequiredLoggedUser().Id);
                return Ok();
            }
        }

        [Authorize(Roles = Roles.Admin)]
        [HttpDelete("DeleteOperator/{id}")]
        public async Task<IActionResult> DeleteOperator(CancellationToken ct, Guid id)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await UserFacade.RemoveUserAsync(id, CurrentUserProvider.GetRequiredLoggedUser().Id, uow, ct);
                return Ok();
            }
        }

        [HttpDelete("DeleteMyself")]
        public async Task<IActionResult> DeleteMyself(CancellationToken ct)
        {
            using (var uow = unitOfWorkProvider.Create())
            {
                await UserFacade.RemoveUserAsync(CurrentUserProvider.GetRequiredLoggedUser().Id, CurrentUserProvider.GetRequiredLoggedUser().Id, uow, ct);
                return Ok();
            }
        }

    }
}
