﻿using Microsoft.AspNetCore.Http;
using Project_Apolus.Common;
using Project_Apolus.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Project_Apolus.CurrentUserProviders
{
    public class CurrentUserProvider : ICurrentUserProvider
    {
        private readonly IHttpContextAccessor context;
        private readonly LoggedUser? loggedUser;

        private ClaimsIdentity? ClaimsIdentity
        {
            get
            {
                var identity = context.HttpContext?.User?.Identity as ClaimsIdentity;
                return (identity?.IsAuthenticated ?? false) ? identity : null;
            }
        }

        public LoggedUser? GetLoggedUser() => loggedUser;
        public LoggedUser GetRequiredLoggedUser() => loggedUser ?? throw new InvalidOperationException("No user logged in.");

        public CurrentUserProvider(IHttpContextAccessor context)
        {
            this.context = context;

            loggedUser = ClaimsIdentity != null ? new LoggedUser(
                Guid.Parse(ClaimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value),
                ClaimsIdentity.FindFirst(ClaimTypes.Email).Value,
                ClaimsIdentity.FindFirst(ClaimTypes.Email).Value,
                ClaimsIdentity.FindAll(ClaimTypes.Role).Select(x => x.Value).ToList(), null)//,
                //ClaimsIdentity.FindFirst(CustomClaimTypes.OrganizationId) == null ? Guid.Parse(ClaimsIdentity.FindFirst(CustomClaimTypes.OrganizationId).Value) : (Guid?)null)
                : null;
        }
    }
}
