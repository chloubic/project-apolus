﻿using Project_Apolus.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.CurrentUserProviders
{
    public interface ICurrentUserProvider
    {
        LoggedUser? GetLoggedUser();
        public LoggedUser GetRequiredLoggedUser();
    }
}
