﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Project_Apolus.Common.Exceptions;
using Project_Apolus.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Project_Apolus.Filters
{
    public class GeneralExceptionFilterAttribute : TypeFilterAttribute
    {
        public GeneralExceptionFilterAttribute() : base(typeof(GeneralExceptionHandler))
        {
        }

        private class GeneralExceptionHandler : ExceptionFilterAttribute
        {
            private readonly ILogger<GeneralExceptionHandler> logger;

            public GeneralExceptionHandler(ILogger<GeneralExceptionHandler> logger)
            {
                this.logger = logger;
            }

            public override Task OnExceptionAsync(ExceptionContext context)
            {
                if (!context.ExceptionHandled)
                {
                    logger.LogError(context.Exception, "Exception caught at global handler.");
                    var result = GetActionResult(context.Exception);
                    context.Result = result;
                    context.ExceptionHandled = true;
                }

                return base.OnExceptionAsync(context);
            }

            private IActionResult GetActionResult<T>(T exception) where T : Exception
            {
                return exception switch
                {
                    ItemNotFoundException notFoundEx => new NotFoundErrorResult(notFoundEx.ItemTypeName, notFoundEx.ItemId),
                    ValidationFailedException validationEx => new ValidationErrorResult(validationEx.ValidationResult),
                    BusinessRuleException businessEx => new BusinessRuleErrorResult(businessEx.LocalizedRuleErrorMessage),
                    BusinessException _ => new BusinessErrorResult("Business error occured.", HttpStatusCode.InternalServerError),
                    _ => (IActionResult)new BusinessErrorResult("Unexpected API error occured.", HttpStatusCode.InternalServerError)
                };
            }
        }
    }
}
