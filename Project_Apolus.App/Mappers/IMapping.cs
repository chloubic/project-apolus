﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Mappers
{
    public interface IMapping
    {
        void ConfigureMaps(IMapperConfigurationExpression mapper);
    }
}
