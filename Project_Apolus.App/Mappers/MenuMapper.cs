﻿using AutoMapper;
using Project_Apolus.BL.Model;
using Project_Apolus.Common.Model.Menu;
using Project_Apolus.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Mappers
{
    public class MenuMapper : IMapping
    {
        public void ConfigureMaps(IMapperConfigurationExpression mapper)
        {
            mapper.CreateMap<Menu, MenuEntity>()
                .ForMember(o => o.Restaurant, r => r.Ignore());
            mapper.CreateMap<MenuEntity, Menu>();
            mapper.CreateMap<Menu, MenuCategoryDto>();
            mapper.CreateMap<MenuItem, MenuItemEntity>()
                .ForMember(o => o.Menu, r => r.Ignore());
            mapper.CreateMap<MenuItemEntity, MenuItem>();
            mapper.CreateMap<MenuItem, MenuItemDto>();
            mapper.CreateMap<UpdateMenuDto, MenuEntity>()
                .ForMember(o => o.Order, r => r.Ignore())
                .ForMember(o => o.Restaurant, r => r.Ignore())
                .ForMember(o => o.RestaurantId, r => r.Ignore());
            mapper.CreateMap<UpdateMenuItemDto, MenuItemEntity>()
                .ForMember(o => o.Menu, r => r.Ignore())
                .ForMember(o => o.MenuId, r => r.Ignore());
        }
    }
}
