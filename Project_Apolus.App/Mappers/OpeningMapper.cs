﻿using AutoMapper;
using Project_Apolus.BL.Model;
using Project_Apolus.Common.Model.Opening;
using Project_Apolus.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Mappers
{
    public class OpeningMapper : IMapping
    {
        public void ConfigureMaps(IMapperConfigurationExpression mapper)
        {
            mapper.CreateMap<Opening, OpeningEntity>()
                .ForMember(o => o.Day, r => r.Ignore())
                .ForMember(o => o.Restaurant, r => r.Ignore());
            mapper.CreateMap<OpeningEntity, Opening>();
            mapper.CreateMap<DayEntity, DayDto>();
            mapper.CreateMap<Opening, OpeningDto>();
        }
    }
}
