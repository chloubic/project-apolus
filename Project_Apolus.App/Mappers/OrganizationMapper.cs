﻿using AutoMapper;
using Project_Apolus.BL.Model;
using Project_Apolus.Common.Model.Organization;
using Project_Apolus.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Mappers
{
    public class OrganizationMapper : IMapping
    {
        public void ConfigureMaps(IMapperConfigurationExpression mapper)
        {
            mapper.CreateMap<Organization, OrganizationEntity>()
                .ForMember(o => o.Users, r => r.Ignore());
            mapper.CreateMap<OrganizationEntity, Organization>();
            mapper.CreateMap<Organization, OrganizationDto>();
            mapper.CreateMap<UpdateOrganizationDto, OrganizationEntity>()
                .ForMember(o => o.Id, r => r.Ignore())
                .ForMember(o => o.Users, r => r.Ignore())
                .ForMember(o => o.UpdatedDate, r => r.Ignore())
                .ForMember(o => o.CreatedDate, r => r.Ignore())
                .ForMember(o => o.IsClientsOrg, r => r.Ignore());
        }
    }
}
