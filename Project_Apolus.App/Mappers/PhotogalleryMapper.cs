﻿using AutoMapper;
using Project_Apolus.BL.Model;
using Project_Apolus.Common.Model.Photogallery;
using Project_Apolus.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Mappers
{
    public class PhotogalleryMapper : IMapping
    {
        public void ConfigureMaps(IMapperConfigurationExpression mapper)
        {
            mapper.CreateMap<Photo, PhotogalleryEntity>()
                .ForMember(o => o.Restaurant, r => r.Ignore());
            mapper.CreateMap<PhotogalleryEntity, Photo>();
        }
    }
}
