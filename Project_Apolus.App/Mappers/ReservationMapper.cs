﻿using AutoMapper;
using Project_Apolus.BL.Model;
using Project_Apolus.Common.Model.Reservations;
using Project_Apolus.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Mappers
{
    public class ReservationMapper : IMapping
    {
        public void ConfigureMaps(IMapperConfigurationExpression mapper)
        {
            mapper.CreateMap<Reservation, ReservationEntity>()
                .ForMember(o => o.Table, r => r.Ignore())
                .ForMember(o => o.User, r => r.Ignore());
            mapper.CreateMap<ReservationEntity, ReservationDto>()
                .ForMember(o => o.UserName, r => r.MapFrom(a => a.User.Name))
                .ForMember(o => o.UserEmail, r => r.MapFrom(a => a.User.Email))
                .ForMember(o => o.UserPhone, r => r.MapFrom(a => a.User.PhoneNumber))
                .ForMember(o => o.UserName, r => r.MapFrom(a => a.User.Name))
                .ForMember(o => o.Table, r => r.MapFrom(a => a.Table.Name))
                .ForMember(o => o.Restaurant, r => r.MapFrom(a => a.Table.Restaurant.Name));


        }
    }
}
