﻿using AutoMapper;
using Project_Apolus.BL.Model;
using Project_Apolus.Common.Model.Photogallery;
using Project_Apolus.Common.Model.Restaurant;
using Project_Apolus.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Mappers
{
    public class RestaurantMapper : IMapping
    {
        public void ConfigureMaps(IMapperConfigurationExpression mapper)
        {
            mapper.CreateMap<RestaurantEntity, Restaurant>()
                .ForMember(o => o.Gallery, r => r.Ignore());
            mapper.CreateMap<Restaurant, RestaurantEntity>()
                .ForMember(o => o.Organization, r => r.Ignore());
            mapper.CreateMap<Restaurant, RestaurantDto>()
                .ForMember(o => o.Status, r => r.Ignore());
            mapper.CreateMap<Restaurant, RestaurantDetailDto>()
                .ForMember(o => o.Opening, r => r.Ignore())
                .ForMember(o => o.Status, r => r.Ignore());
            mapper.CreateMap<CreateRestaurantDto, Restaurant>()
                .ForMember(o => o.Id, r => r.Ignore())
                .ForMember(o => o.UpdatedDate, r => r.Ignore())
                .ForMember(o => o.CreatedDate, r => r.Ignore())
                .ForMember(o => o.Gallery, r => r.Ignore())
                .ForMember(o => o.OrganizationId, r => r.Ignore());
            mapper.CreateMap<UpdateRestaurantDto, Restaurant>()
                .ForMember(o => o.Id, r => r.Ignore())
                .ForMember(o => o.OrganizationId, r => r.Ignore())
                .ForMember(o => o.CreatedDate, r => r.Ignore())
                .ForMember(o => o.UpdatedDate, r => r.Ignore())
                .ForMember(o => o.Gallery, r => r.Ignore());
            mapper.CreateMap<UpdateRestaurantDto, RestaurantEntity>()
                .ForMember(o => o.Id, r => r.Ignore())
                .ForMember(o => o.Organization, r => r.Ignore())
                .ForMember(o => o.OrganizationId, r => r.Ignore())
                .ForMember(o => o.UpdatedDate, r => r.Ignore())
                .ForMember(o => o.CreatedDate, r => r.Ignore());
        }
    }
}
