﻿using AutoMapper;
using Project_Apolus.BL.Model;
using Project_Apolus.Common.Model.Tables;
using Project_Apolus.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Mappers
{
    public class TableMapper : IMapping
    {
        public void ConfigureMaps(IMapperConfigurationExpression mapper)
        {
            mapper.CreateMap<Table, TableEntity>()
                .ForMember(o => o.Restaurant, r => r.Ignore())
                .ForMember(o => o.Reservations, r => r.Ignore());
            mapper.CreateMap<TableEntity, Table>();
            mapper.CreateMap<Table, TableDto>();
        }
    }
}
