﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.Middleware
{
    //from https://exceptionnotfound.net/using-middleware-to-log-requests-and-responses-in-asp-net-core/
    public class RequestLoggingMiddleware
    {
        private readonly RequestDelegate nextMiddleware;
        private readonly ILogger<RequestLoggingMiddleware> logger;

        public RequestLoggingMiddleware(RequestDelegate nextMiddleware, ILogger<RequestLoggingMiddleware> logger)
        {
            this.nextMiddleware = nextMiddleware;
            this.logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            var requestUrl = $"{context.Request.Scheme}//{context.Request.Host}{context.Request.Path}";
            var queryString = context.Request.QueryString;
            var request = await GetFormattedBody(context.Request);
            logger.LogTrace("API request: {path}{queryString} | {body}", requestUrl, queryString, request);

            await nextMiddleware(context);

            async Task<string> GetFormattedBody(HttpRequest httpRequest)
            {
                var body = httpRequest.Body;
                //This line allows us to set the reader for the request back at the beginning of its stream.
                httpRequest.EnableBuffering();

                // request length is long! 
                var buffer = new byte[Convert.ToInt32(httpRequest.Body.Length)];

                //...Then we copy the entire request stream into the new buffer.
                await httpRequest.Body.ReadAsync(buffer, 0, buffer.Length);

                //We convert the byte[] into a string using UTF8 encoding...
                var bodyAsText = Encoding.UTF8.GetString(buffer);

                //..and finally, assign the read body back to the request body, which is allowed because of EnableRewind()
                httpRequest.Body = body;
                return bodyAsText;
            }
        }
    }
}
