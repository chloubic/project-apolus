﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Middleware
{
    //from https://exceptionnotfound.net/using-middleware-to-log-requests-and-responses-in-asp-net-core/
    public class ResponseLoggingMiddleware
    {
        private readonly RequestDelegate nextMiddleware;
        private readonly ILogger<ResponseLoggingMiddleware> logger;

        public ResponseLoggingMiddleware(RequestDelegate nextMiddleware, ILogger<ResponseLoggingMiddleware> logger)
        {
            this.nextMiddleware = nextMiddleware;
            this.logger = logger;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var originalBodyStream = httpContext.Response.Body;
            await using var responseBody = new MemoryStream();
            httpContext.Response.Body = responseBody;
            await nextMiddleware(httpContext);
            var formattedResponse = await FormatResponse(httpContext.Response);
            logger.LogTrace("API response: [{httpStatusCode}] {response}", httpContext.Response.StatusCode, formattedResponse);
            await responseBody.CopyToAsync(originalBodyStream);

            async Task<string> FormatResponse(HttpResponse httpResponse)
            {
                httpResponse.Body.Seek(0, SeekOrigin.Begin);
                var body = await new StreamReader(httpResponse.Body).ReadToEndAsync();
                httpResponse.Body.Seek(0, SeekOrigin.Begin);
                return body;
            }
        }
    }
}
