﻿using Microsoft.AspNetCore.Mvc;
using Project_Apolus.Common.ResponseObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Project_Apolus.Results
{
    public class BusinessErrorResult : ObjectResult
    {
        public BusinessErrorResult(string message, HttpStatusCode statusCode)
            : base(new ErrorResponseObject(message))
        {
            StatusCode = (int?)statusCode;
        }
    }
}
