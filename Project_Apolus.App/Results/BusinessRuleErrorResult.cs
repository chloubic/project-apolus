﻿using Microsoft.AspNetCore.Mvc;
using Project_Apolus.Common.ResponseObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Results
{
    public class BusinessRuleErrorResult : ObjectResult
    {
        public BusinessRuleErrorResult(string localizedRuleErrorMessage)
            : base(new BusinessRuleErrorResponseObject(localizedRuleErrorMessage))
        {
            StatusCode = 599;
        }
    }
}
