﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Results
{
    public class FieldValidationMessage
    {
        public string FieldName { get; }
        public string Message { get; }

        public FieldValidationMessage(string fieldName, string message)
        {
            FieldName = fieldName;
            Message = message;
        }
    }
}
