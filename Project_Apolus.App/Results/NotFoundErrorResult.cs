﻿using Microsoft.AspNetCore.Mvc;
using Project_Apolus.Common.ResponseObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Results
{
    public class NotFoundErrorResult : ObjectResult
    {
        public NotFoundErrorResult(string itemTypeName, Guid itemId)
            : base(new NotFoundErrorResponseObject(itemTypeName, itemId))
        {
            StatusCode = 404;
        }
    }
}
