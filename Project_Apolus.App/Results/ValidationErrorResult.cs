﻿using Microsoft.AspNetCore.Mvc;
using Project_Apolus.Common.ResponseObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Results
{
    public class ValidationErrorResult : ObjectResult
    {
        public ValidationErrorResult(ValidationResult validationResult)
            : base(new ValidationErrorResponseObject(validationResult))
        {
            StatusCode = 422;
        }
    }
}
