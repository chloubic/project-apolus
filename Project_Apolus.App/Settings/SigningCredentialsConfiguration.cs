﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Apolus.Settings
{
    public class SigningCredentialsConfiguration
    {
        private string jwtKey = null!;

        public string JwtKey
        {
            get => jwtKey;
            set => jwtKey = value ?? throw new ArgumentNullException(nameof(jwtKey));
        }
    }
}
