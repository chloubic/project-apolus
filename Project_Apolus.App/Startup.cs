using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Project_Apolus.BL.EmailProviders;
using Project_Apolus.BL.Facades;
using Project_Apolus.BL.Logger;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Configuration;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Configuration;
using Project_Apolus.DAL;
using Project_Apolus.DAL.Entities.Identity;
using Project_Apolus.DAL.Repositories;
using Project_Apolus.DAL.Storage;
using Project_Apolus.Filters;
using Project_Apolus.Settings;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Project_Apolus
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(
                    connectionString);
            });

            services.AddControllersWithViews();
            services.AddIdentity<AppUser, AppRole>()
                .AddEntityFrameworkStores<AppDbContext>();
            services.Configure<IdentityOptions>(ConfigureIdentityOptions);

            var signingCredentialsConfiguration = Configuration.GetSection("SigningCredentials").Get<SigningCredentialsConfiguration>();

            services.AddAuthentication(ConfigureAuthenticationOptions)
                .AddJwtBearer(options =>
                {
                    // Configure JWT Bearer Auth to expect our security key
                    options.TokenValidationParameters =
                        GetJwtTokenValidationParameters(signingCredentialsConfiguration);
                });
            services.AddControllers(opt => opt.Filters.Add<GeneralExceptionFilterAttribute>())
                .AddNewtonsoftJson(options =>
                {
                    JsonSerializationConfiguration.Configure(options.SerializerSettings);
                }
                );
            services.AddSwaggerGen(ConfigureSwagger);

            var emailSettings = Configuration.GetSection("EmailSettings");
            services.Configure<EmailConfiguration>(emailSettings);

            services.AddInfrastructure();
            services.AddUnitOfWork(connectionString);
            services.AddAllRepositoriesAsTransient<UserRepository>();
            services.AddAllFacadesAsTransient<AuthenticationFacade>();
            services.AddStorageAsTransient<LocalStorageProvider>();
            services.AddEmailSenderAsTransient<EmailClientSender>();
            services.AddLoggerAsTransient<Logger>();
            services.AddAutoMapper();
            services.AddSingleton(signingCredentialsConfiguration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();
            app.UseRouting();

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v2/swagger.json", "Project Apolus API"); });

            app.UseAuthentication();

            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Welcome}/{action=Index}/{id?}");
            });
        }

        private void ConfigureIdentityOptions(IdentityOptions options)
        {
            // Set your identity Settings here (password length, etc.)
            options.User.RequireUniqueEmail = true;
            options.Password.RequireDigit = false;
            options.Password.RequiredLength = 6;
            options.Password.RequireLowercase = false;
            options.Password.RequiredUniqueChars = 0;
            options.Password.RequireNonAlphanumeric = false;
            options.Password.RequireUppercase = false;
        }

        private void ConfigureAuthenticationOptions(AuthenticationOptions options)
        {
            // Identity made Cookie authentication the default.
            // However, we want JWT Bearer Auth to be the default.
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }

        private static TokenValidationParameters GetJwtTokenValidationParameters(SigningCredentialsConfiguration signingCredentialsConfiguration)
        {
            return new TokenValidationParameters
            {
                LifetimeValidator =
                    (before, expires, token, param) => expires > DateTime.UtcNow,
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateActor = false,
                ValidateLifetime = true,
                RequireSignedTokens = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                    signingCredentialsConfiguration.JwtKey))
            };
        }

        private void ConfigureSwagger(SwaggerGenOptions c)
        {
            c.SwaggerDoc("v2", new OpenApiInfo { Title = "Project Apolus API", Version = "v2" });
            var scheme = new OpenApiSecurityScheme() { Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"", Name = "Authorization", In = ParameterLocation.Header, Type = SecuritySchemeType.ApiKey };

            c.OperationFilter<SecurityRequirementsOperationFilter>();
            c.AddSecurityDefinition("oauth2", scheme);
        }
    }
}
