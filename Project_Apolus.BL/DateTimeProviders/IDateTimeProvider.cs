﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.BL.DateTimeProviders
{
    public interface IDateTimeProvider
    {
        DateTime Now { get; }
    }
}
