﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.BL.DateTimeProviders
{
    public class LocalDateTimeProvider : IDateTimeProvider
    {
        public DateTime Now => DateTime.Now.AddHours(1);
    }
}
