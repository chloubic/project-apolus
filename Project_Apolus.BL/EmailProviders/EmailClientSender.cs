﻿using Microsoft.Extensions.Options;
using Project_Apolus.Common.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.BL.EmailProviders
{
    public class EmailClientSender : IEmailClientSender
    {
        private readonly EmailConfiguration _emailSettings;

        private const string MESSAGE_SUBJECT = "Nová zpráva na portále OSS";

        private const string RESET_TOKEN_SUBJECT = "Reset účtu na portále OSS";
        public EmailClientSender(IOptions<EmailConfiguration> emailSettings)
        {
            this._emailSettings = emailSettings.Value;
        }

        public async Task SendMessageNotification(string toEmail, string serviceName, string fromName, string fromEmail)
        {
            var htmlContent = $"Upozornění na novou zprávu od {fromName} ({fromEmail}) týkající se služby {serviceName}.<br><br><a href='{_emailSettings.BaseURL}/login'>Přihlašte se</a>";
            await SendHtmlSingleEmailAsync(_emailSettings.AdminEmail, toEmail, MESSAGE_SUBJECT, htmlContent);
        }

        private async Task SendHtmlSingleEmailAsync(string emailFrom, string emailTo, string subject, string htmlContent)
        {
            var client = new SendGridClient(_emailSettings.ApiKey);
            var from = new EmailAddress(emailFrom);
            var to = new EmailAddress(emailTo);

            var msg = MailHelper.CreateSingleEmail(from, to, subject, null, htmlContent);
            Response response;
            try
            {
                response = await client.SendEmailAsync(msg);
            }
            catch (HttpRequestException e)
            {
                throw e;
            }
        }

        public async Task SendRecoveryToken(string email, string token)
        {
            var htmlContent = $"Token pro resetování hesla účtu s emailovou adresou {email}: <a href='{_emailSettings.BaseURL}/recover/{token}'>Obnovení hesla</a> (platný 1 hodinu).";
            await SendHtmlSingleEmailAsync(_emailSettings.AdminEmail, email, RESET_TOKEN_SUBJECT, htmlContent);
        }
    }
}
