﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.BL.EmailProviders
{
    public interface IEmailClientSender
    {
        Task SendMessageNotification(string toEmail, string serviceName, string fromName, string fromEmail);
        Task SendRecoveryToken(string email, string token);
    }
}
