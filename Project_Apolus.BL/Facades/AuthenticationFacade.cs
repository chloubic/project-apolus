﻿using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Model.Auth;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public class AuthenticationFacade : IAuthenticationFacade
    {
        private readonly IUserRepository userRepository;

        public AuthenticationFacade(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public async Task ChangePasswordAsync(ChangePasswordDto dto, Guid userId)
        {
            await userRepository.ChangePasswordAsync(dto, userId);
        }

        //public Task MarkOneTimePasswordUsedAsync(AppUser user)
        //{
        //    user.PasswordState = PasswordState.OneTimePasswordUsed;
        //    return userRepository.UpdateAsync(user);
        //}
    }
}
