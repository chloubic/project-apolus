﻿using Project_Apolus.Common.Model.Auth;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public interface IAuthenticationFacade : IFacade
    {
        Task ChangePasswordAsync(ChangePasswordDto dto, Guid userId);
    }
}
