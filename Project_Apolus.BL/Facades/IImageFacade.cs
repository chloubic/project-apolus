﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public interface IImageFacade : IFacade
    {
        Task<string> UploadImage(IFormFile file);
        Task<Byte[]> GetImageAsync(string fileName);
    }
}
