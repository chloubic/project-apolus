﻿using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model;
using Project_Apolus.Common.Model.Menu;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public interface IMenuFacade : IFacade
    {

        Task<IList<MenuCategoryDto>> GetMenuCategories(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId);

        Task<IList<MenuItemDto>> GetCategoryItems(UnitOfWorkId uow, CancellationToken ct, Guid categoryId);

        Task CreateMenuCategory(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId, CreateMenuDto categories, Guid loggedUser);
        
        Task CreateMenuItems(UnitOfWorkId uow, CancellationToken ct, Guid menuId, CreateMenuItemDto items, Guid loggedUser);
        Task UpdateMenuCategory(UnitOfWorkId uow, CancellationToken ct, UpdateMenuDto categories, Guid id);
        Task UpdateMenuItem(UnitOfWorkId uow, CancellationToken ct, UpdateMenuItemDto items, Guid id);
        Task DeleteMenuItem(UnitOfWorkId uow, CancellationToken ct, Guid itemId, Guid user);
        Task DeleteMenuCategory(UnitOfWorkId uow, CancellationToken ct, Guid categoryId, Guid user);
        Task ChangeMenuCategoryOrder(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId, IList<ItemOrderUpdateDto> items, Guid userId);
        Task ChangeMenuItemsOrder(UnitOfWorkId uow, CancellationToken ct, Guid menuId, IList<ItemOrderUpdateDto> items, Guid userId);
        Task<MenuCategoryDto> GetCategoryInfoAsync(UnitOfWorkId uow, CancellationToken ct, Guid id);
        Task<MenuItemDto> GetItemInfoAsync(UnitOfWorkId uow, CancellationToken ct, Guid id);
    }
}
