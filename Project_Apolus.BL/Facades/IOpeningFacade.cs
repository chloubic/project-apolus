﻿using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Opening;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public interface IOpeningFacade : IFacade
    {
        Task UpdateRestaurantOpeningHours(IList<OpeningDto> openingDtos, Guid id, Guid loggedUserId, UnitOfWorkId uow, CancellationToken ct);
        Task<IList<OpeningDto>> GetRestaurantOpeningHours(Guid id, Guid loggedUserId, UnitOfWorkId uow, CancellationToken ct);
        Task<IList<DayDto>> GetDays(UnitOfWorkId uow, CancellationToken ct);
    }
}
