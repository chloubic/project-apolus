﻿using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Organization;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public interface IOrganizationFacade : IFacade
    {

        Task UpdateOrganizationAsync(UpdateOrganizationDto updateOrganizationDto, Guid loggerUserId, UnitOfWorkId uow, CancellationToken ct);
        Task<OrganizationDto> GetOrganizationAsync(Guid id, UnitOfWorkId uow, CancellationToken ct);
    }
}
