﻿using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Photogallery;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public interface IPhotogalleryFacade : IFacade
    {
        Task UpdateRestaurantPhotos(PhotosDto photosDto, Guid id, Guid loggedUserID, UnitOfWorkId uow, CancellationToken ct);
        Task<IList<string>> GetRestaurantPhotos(Guid id, UnitOfWorkId uow, CancellationToken ct);
    }
}
