﻿using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Reservations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public interface IReservationsFacade : IFacade
    {
        Task CreateReservationAsync(UnitOfWorkId uow, CancellationToken ct, Guid tableId, CreateReservationDto reservationDto, Guid userId);
        Task DeleteReservationAsync(UnitOfWorkId uow, CancellationToken ct, Guid reservationId, Guid userId);
        Task<IList<ReservationDto>> GetMyReservationsAsync(UnitOfWorkId uow, CancellationToken ct, Guid id);
        Task<IList<ReservationDto>> GetRestaurantReservationsAsync(UnitOfWorkId uow, CancellationToken ct, Guid id1, Guid id2);
    }
}
