﻿using Project_Apolus.Common.Filters;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Restaurant;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public interface IRestaurantFacade : IFacade
    {
        Task CreateRestaurantAsync(CreateRestaurantDto createRestaurantDto, Guid id, UnitOfWorkId uow, CancellationToken ct);
        Task<IList<RestaurantDto>> GetFilteredRestaurantsAsync(RestaurantFilter filter, UnitOfWorkId uow, CancellationToken ct);
        Task UpdateRestaurantAsync(UpdateRestaurantDto updateRestaurantDto, Guid loggedUserId, UnitOfWorkId uow, CancellationToken ct);
        Task<IList<RestaurantDto>> GetRestaurantsByOrganization(Guid id, UnitOfWorkId uow, CancellationToken ct);
        Task<RestaurantDetailDto> GetRestaurantDetail(Guid id, UnitOfWorkId uow, CancellationToken ct);
        Task DeleteRestaurantAsync(Guid id, Guid loggedUserId, UnitOfWorkId uow, CancellationToken ct);
        Task<int> GetFilteredRestaurantsCountAsync(RestaurantFilter filter, UnitOfWorkId uow, CancellationToken ct);
    }
}
