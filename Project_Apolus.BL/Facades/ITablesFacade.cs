﻿using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Tables;
using Project_Apolus.Common.Params;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public interface ITablesFacade : IFacade
    {
        Task<IList<TableDto>> GetAllTablesAsync(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId, Guid userId);
        Task CreateTableAsync(UnitOfWorkId uow, CancellationToken ct, Guid id1, CreateTableDto table, Guid id2);
        Task DeleteTableAsync(UnitOfWorkId uow, CancellationToken ct, Guid id1, Guid id2);
        Task<IList<TableDto>> GetAvailableTablesAsync(UnitOfWorkId uow, CancellationToken ct, Guid id, TablesQuery param);
    }
}
