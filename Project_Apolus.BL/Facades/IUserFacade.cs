﻿using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public interface IUserFacade : IFacade
    {

        Task CreateClient(CreateClientDto createClientDto, UnitOfWorkId uow, CancellationToken ct);
        Task CreateAdmin(CreateAdminDto createAdminDto, UnitOfWorkId uow, CancellationToken ct);
        Task CreateOperator(CreateClientDto createOperatorDto, UnitOfWorkId uow, CancellationToken ct, Guid id);
        Task RemoveUserAsync(Guid id, Guid loggedUserId, UnitOfWorkId uow, CancellationToken ct);
        Task<UserDto> GetUserInfo(UnitOfWorkId uow, CancellationToken ct, Guid id);
        Task UpdateUserInfoAsync(UnitOfWorkId uow, CancellationToken ct, UserDto user, Guid id);
        Task<IList<OperatorDto>> GetOperatorsAsync(UnitOfWorkId uow, CancellationToken ct, Guid id);
    }
}
