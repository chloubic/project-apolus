﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public class ImageFacade : IImageFacade
    {
        private readonly IStorage storage;
        public ImageFacade(IStorage storage)
        {
            this.storage = storage;
        }

        public async Task<string> UploadImage(IFormFile file)
        {
            var extension = Path.GetExtension(file.FileName);
            var fileName = Guid.NewGuid() + extension;
            await storage.UploadAsync(fileName, file.OpenReadStream());
            return fileName;
        }

        public async Task<Byte[]> GetImageAsync(string fileName)
        {
            return await storage.GetAsync(fileName);
        }
    }
}
