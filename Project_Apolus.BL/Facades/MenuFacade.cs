﻿using AutoMapper;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Exceptions;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Menu;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public class MenuFacade : IMenuFacade
    {

        private readonly IMenuRepository MenuRepository;

        private readonly IMenuItemRepository MenuItemRepository;

        private readonly IMapper Mapper;

        private readonly IUserRepository UserRepository;

        private readonly IDateTimeProvider DateTimeProvider;

        public MenuFacade(IMenuRepository menuRepository, IMapper mapper, IUserRepository userRepository, IDateTimeProvider dateTimeProvider,
            IMenuItemRepository menuItemRepository)
        {
            MenuRepository = menuRepository;
            MenuItemRepository = menuItemRepository;
            Mapper = mapper;
            UserRepository = userRepository;
            DateTimeProvider = dateTimeProvider;
        }

        public async Task<IList<MenuCategoryDto>> GetMenuCategories(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId)
        {
            return Mapper.Map<IList<MenuCategoryDto>>(await MenuRepository.GetMenuCategoriesByRestaurantId(uow, ct, restaurantId));
        }

        public async Task<IList<MenuItemDto>> GetCategoryItems(UnitOfWorkId uow, CancellationToken ct, Guid categoryId)
        {
            return Mapper.Map<IList<MenuItemDto>>(await MenuRepository.GetCategoryItemsByCategoryId(uow, ct, categoryId));
        }

        public async Task CreateMenuCategory(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId, CreateMenuDto categories, Guid loggedUser)
        {
            var user = await UserRepository.GetByIdAsync(loggedUser, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, loggedUser);
            if (await MenuRepository.HasRightsToCreateMenu(uow, loggedUser, restaurantId))
            {
                var domainObject = new Menu(restaurantId, categories.Name, categories.Order);
                await MenuRepository.InsertAsync(domainObject, uow, ct);
            }
            else throw new UnauthorizedAccessException();
        }

        public async Task CreateMenuItems(UnitOfWorkId uow, CancellationToken ct, Guid menuId, CreateMenuItemDto items, Guid loggedUser)
        {
            var user = await UserRepository.GetByIdAsync(loggedUser, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, loggedUser);
            if (await MenuItemRepository.HasRightsToCreateMenuItems(uow, loggedUser, menuId))
            {
                var domainObject = new MenuItem(menuId, items.Name, items.Description, items.Weight, items.Price, items.Order);
                await MenuItemRepository.InsertAsync(domainObject, uow, ct);
            }
            else throw new UnauthorizedAccessException();
        }

        public async Task UpdateMenuCategory(UnitOfWorkId uow, CancellationToken ct, UpdateMenuDto categories, Guid id)
        {
            var user = await UserRepository.GetByIdAsync(id, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, id);
            if (await MenuRepository.HasRightsToUpdateMenu(uow, id, categories.Id))
            {
                await MenuRepository.UpdateMenuAsync(categories, uow, ct);
            }
            else throw new UnauthorizedAccessException();
        }

        public async Task UpdateMenuItem(UnitOfWorkId uow, CancellationToken ct, UpdateMenuItemDto items, Guid id)
        {
            var user = await UserRepository.GetByIdAsync(id, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, id);
            if (await MenuItemRepository.HasRightsToUpdateMenuItems(uow, id, items.Id))
            {
                await MenuItemRepository.UpdateMenuItemAsync(items, uow, ct);
            }
            else throw new UnauthorizedAccessException();
        }

        public async Task DeleteMenuItem(UnitOfWorkId uow, CancellationToken ct, Guid itemId, Guid userId)
        {
            var user = await UserRepository.GetByIdAsync(userId, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, userId);
            if (await MenuItemRepository.HasRightsToUpdateMenuItems(uow, userId, itemId))
            {
                await MenuItemRepository.DeleteMenuItemAsync(uow, ct, itemId);
            }
            else throw new UnauthorizedAccessException();
        }

        public async Task DeleteMenuCategory(UnitOfWorkId uow, CancellationToken ct, Guid categoryId, Guid userId)
        {
            var user = await UserRepository.GetByIdAsync(userId, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, userId);
            if (await MenuRepository.HasRightsToUpdateMenu(uow, userId, categoryId))
            {
                await MenuRepository.DeleteMenuAsync(uow, ct, categoryId);
            }
            else throw new UnauthorizedAccessException();
        }

        public async Task ChangeMenuCategoryOrder(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId, IList<ItemOrderUpdateDto> items, Guid userId)
        {
            var user = await UserRepository.GetByIdAsync(userId, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, userId);
            if (await MenuRepository.HasRightsToCreateMenu(uow, userId, restaurantId))
            {
                await MenuRepository.ChangeMenuCategoryOrderAsync(uow, ct, restaurantId, items);
            }
            else throw new UnauthorizedAccessException();
        }

        public async Task ChangeMenuItemsOrder(UnitOfWorkId uow, CancellationToken ct, Guid menuId, IList<ItemOrderUpdateDto> items, Guid userId)
        {
            var user = await UserRepository.GetByIdAsync(userId, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, userId);
            if (await MenuItemRepository.HasRightsToCreateMenuItems(uow, userId, menuId))
            {
                await MenuItemRepository.ChangeMenuItemsOrderAsync(uow, ct, menuId, items);
            }
            else throw new UnauthorizedAccessException();
        }

        public async Task<MenuCategoryDto> GetCategoryInfoAsync(UnitOfWorkId uow, CancellationToken ct, Guid id)
        {
            return Mapper.Map<MenuCategoryDto>(await MenuRepository.GetByIdAsync(id, uow, ct));
        }

        public async Task<MenuItemDto> GetItemInfoAsync(UnitOfWorkId uow, CancellationToken ct, Guid id)
        {
            return Mapper.Map<MenuItemDto>(await MenuItemRepository.GetByIdAsync(id, uow, ct));
        }
    }
}
