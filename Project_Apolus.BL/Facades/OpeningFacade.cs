﻿using AutoMapper;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Exceptions;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Opening;
using Project_Apolus.Common.ResponseObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public class OpeningFacade : IOpeningFacade
    {

        private readonly IUserRepository UserRepository;
        private readonly IRestaurantRepository RestaurantRepository;
        private readonly IOpeningRepository OpeningRepository;
        private readonly IMapper Mapper;

        public OpeningFacade(IUserRepository userRepository, IOpeningRepository openingRepository, IRestaurantRepository restaurantRepository, IMapper mapper)
        {
            UserRepository = userRepository;
            RestaurantRepository = restaurantRepository;
            OpeningRepository = openingRepository;
            Mapper = mapper;
        }

        public async Task UpdateRestaurantOpeningHours(IList<OpeningDto> openingDtos, Guid id, Guid loggedUserId, UnitOfWorkId uow, CancellationToken ct)
        {
            await ValidateUserPermissions(id, loggedUserId, uow, ct);

            var openingList = new List<Opening>();

            foreach(var opening in openingDtos)
            {
                openingList.Add(
                    new Opening(Guid.Empty, opening.DayId, id, opening.From, opening.To)   
                );
            }

            await OpeningRepository.EraseRestaurantOpeningHours(id, uow, ct);
            await OpeningRepository.UpdateRestaurantOpeningHours(openingList, uow, ct);
        }

        public async Task<IList<OpeningDto>> GetRestaurantOpeningHours(Guid id, Guid loggedUserId, UnitOfWorkId uow, CancellationToken ct)
        {
            await ValidateUserPermissions(id, loggedUserId, uow, ct);

            return Mapper.Map<IList<OpeningDto>>(await OpeningRepository.GetRestaurantOpeningHours(id, uow, ct));
        }

        private async Task ValidateUserPermissions(Guid id, Guid loggedUserId, UnitOfWorkId uow, CancellationToken ct)
        {
            var user = await UserRepository.GetByIdAsync(loggedUserId, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, id);
            var restaurant = await RestaurantRepository.GetByIdAsync(id, uow, ct);
            if (restaurant == null) throw new ItemNotFoundException(Constants.RESTAURANT_ITEM_NAME, id);
            if (!user.OrganizationId.Equals(restaurant.OrganizationId))
                throw new ValidationFailedException(new ValidationResult("Logged user does not belong into restaurant holder organization!"));
        }

        public async Task<IList<DayDto>> GetDays(UnitOfWorkId uow, CancellationToken ct)
        {
            return await OpeningRepository.GetDays(uow, ct);
        }
    }
}
