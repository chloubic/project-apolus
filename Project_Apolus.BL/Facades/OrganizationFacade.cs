﻿using AutoMapper;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Exceptions;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Organization;
using Project_Apolus.Common.ResponseObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public class OrganizationFacade : IOrganizationFacade
    {

        private readonly IOrganizationRepository OrganizationRepository;
        private readonly IUserRepository UserRepository;
        private readonly IDateTimeProvider DateTimeProvider;
        private readonly IMapper Mapper;

        public OrganizationFacade(IOrganizationRepository organizationRepository, IUserRepository userRepository, IDateTimeProvider dateTimeProvider, IMapper mapper)
        {
            OrganizationRepository = organizationRepository;
            UserRepository = userRepository;
            DateTimeProvider = dateTimeProvider;
            Mapper = mapper;
        }

        public async Task<OrganizationDto> GetOrganizationAsync(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var user = await UserRepository.GetByIdAsync(id, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, id);
            var organization = await OrganizationRepository.GetByIdAsync(user.OrganizationId, uow, ct);
            return Mapper.Map<OrganizationDto>(organization);
        }

        public async Task UpdateOrganizationAsync(UpdateOrganizationDto updateOrganizationDto, Guid loggerUserId, UnitOfWorkId uow, CancellationToken ct)
        {
            var user = await UserRepository.GetByIdAsync(loggerUserId, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, loggerUserId);
            await OrganizationRepository.UpdateOrganization(uow, ct, updateOrganizationDto, user.OrganizationId);
        }
    }
}
