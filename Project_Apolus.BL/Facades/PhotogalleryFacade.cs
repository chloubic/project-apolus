﻿using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Exceptions;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Photogallery;
using Project_Apolus.Common.ResponseObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public class PhotogalleryFacade : IPhotogalleryFacade
    {

        private readonly IUserRepository UserRepository;
        private readonly IRestaurantRepository RestaurantRepository;
        private readonly IPhotogalleryRepository PhotogalleryRepository;

        public PhotogalleryFacade(IUserRepository userRepository, IRestaurantRepository restaurantRepository, IPhotogalleryRepository photogalleryRepository)
        {
            UserRepository = userRepository;
            RestaurantRepository = restaurantRepository;
            PhotogalleryRepository = photogalleryRepository;
        }

        public async Task<IList<string>> GetRestaurantPhotos(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var photoList = await PhotogalleryRepository.GetRestaurantPhotos(id, uow, ct);
            var urlList = new List<string>();

            foreach (var photo in photoList)
            {
                urlList.Add(photo.Url);
            }

            return urlList;
        }

        public async Task UpdateRestaurantPhotos(PhotosDto photosDto, Guid id, Guid loggedUserID, UnitOfWorkId uow, CancellationToken ct)
        {
            var user = await UserRepository.GetByIdAsync(loggedUserID, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, loggedUserID);
            var restaurant = await RestaurantRepository.GetByIdAsync(id, uow, ct);
            if (restaurant == null) throw new ItemNotFoundException(Constants.RESTAURANT_ITEM_NAME, id);
            if (!user.OrganizationId.Equals(restaurant.OrganizationId)) 
                throw new ValidationFailedException(new ValidationResult("Logged user does not belong into restaurant holder organization!"));

            var photoList = new List<Photo>();

            foreach(var photo in photosDto.Urls)
            {
                photoList.Add(
                    new Photo(Guid.Empty, id, photo)
                );
            }

            await PhotogalleryRepository.EraseGalleryAsync(id, uow, ct);
            await PhotogalleryRepository.UpdateGalleryAsync(photoList, uow, ct); 
        }
    }
}
