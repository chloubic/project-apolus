﻿using AutoMapper;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Exceptions;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Reservations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public class ReservationsFacade : IReservationsFacade
    {

        private readonly IReservationRepository ReservationRepository;
        private readonly ITableRepository TableRepository;
        private readonly IUserRepository UserRepository;
        private readonly IDateTimeProvider DateTimeProvider;
        private readonly IMapper Mapper;

        public ReservationsFacade(IReservationRepository reservationRepository, IUserRepository userRepository, IDateTimeProvider dateTimeProvider, 
            IMapper mapper, ITableRepository tableRepository)
        {
            ReservationRepository = reservationRepository;
            TableRepository = tableRepository;
            UserRepository = userRepository;
            DateTimeProvider = dateTimeProvider;
            Mapper = mapper;
        }

        public async Task CreateReservationAsync(UnitOfWorkId uow, CancellationToken ct, Guid tableId, CreateReservationDto reservationDto, Guid userId)
        {
            if (await ReservationRepository.CanReservate(tableId, reservationDto.Date, reservationDto.From, reservationDto.To, uow, ct))
            {
                var reservation = new Reservation()
                {
                    Id = Guid.Empty,
                    TableId = tableId,
                    UserId = userId,
                    Date = reservationDto.Date,
                    From = reservationDto.From,
                    To = reservationDto.To
                };
                await ReservationRepository.CreateAsync(uow, ct, reservation);
            }
            else throw new BusinessException("");
        }

        public async Task DeleteReservationAsync(UnitOfWorkId uow, CancellationToken ct, Guid reservationId, Guid userId)
        {
            var user = await UserRepository.GetByIdAsync(userId, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, userId);
            if (await ReservationRepository.CanDelete(userId, reservationId, uow, ct))
                await ReservationRepository.DeleteAsync(uow, ct, reservationId, userId);
            else throw new UnauthorizedAccessException();
        }

        public async Task<IList<ReservationDto>> GetMyReservationsAsync(UnitOfWorkId uow, CancellationToken ct, Guid id)
        {
            return await ReservationRepository.GetMyReservationsAsync(uow, ct, id);
        }

        public async Task<IList<ReservationDto>> GetRestaurantReservationsAsync(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId, Guid userId)
        {
            var user = await UserRepository.GetByIdAsync(userId, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, userId);
            if (await TableRepository.CanManipulate(userId, restaurantId, uow, ct))
                return await ReservationRepository.GetRestaurantReservationAsync(uow, ct, restaurantId);
            else throw new UnauthorizedAccessException();
            
        }
    }
}
