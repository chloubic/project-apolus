﻿using AutoMapper;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Exceptions;
using Project_Apolus.Common.Filters;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Opening;
using Project_Apolus.Common.Model.Photogallery;
using Project_Apolus.Common.Model.Restaurant;
using Project_Apolus.Common.ResponseObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public class RestaurantFacade : IRestaurantFacade
    {

        private readonly IRestaurantRepository RestaurantRepository;
        private readonly IUserRepository UserRepository;
        private readonly IDateTimeProvider DateTimeProvider;
        private readonly IOpeningRepository OpeningRepository;
        private readonly IMapper Mapper;

        public RestaurantFacade(IRestaurantRepository restaurantRepository, IDateTimeProvider dateTimeProvider, IUserRepository userRepository, IMapper mapper,
            IOpeningRepository openingRepository)
        {
            RestaurantRepository = restaurantRepository;
            UserRepository = userRepository;
            OpeningRepository = openingRepository;
            DateTimeProvider = dateTimeProvider;
            Mapper = mapper;
        }

        public async Task CreateRestaurantAsync(CreateRestaurantDto createRestaurantDto, Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var user = await UserRepository.GetByIdAsync(id, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, id);
            var restaurant = Mapper.Map<Restaurant>(createRestaurantDto);
            restaurant.SetOrganizationId(user.OrganizationId);
            await RestaurantRepository.CreateAsync(restaurant, uow, ct);
        }

        public async Task DeleteRestaurantAsync(Guid id, Guid loggerUserId, UnitOfWorkId uow, CancellationToken ct)
        {
            var user = await UserRepository.GetByIdAsync(loggerUserId, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, id);
            var restaurant = await RestaurantRepository.GetByIdAsync(id, uow, ct);
            if (restaurant == null) throw new ItemNotFoundException(Constants.RESTAURANT_ITEM_NAME, id);
            if (!user.OrganizationId.Equals(restaurant.OrganizationId))
                throw new ValidationFailedException(new ValidationResult("Logged user does not belong into restaurant holder organization!"));
            await RestaurantRepository.DeleteAsync(id, uow, ct);
        }

        public async Task<IList<RestaurantDto>> GetFilteredRestaurantsAsync(RestaurantFilter filter, UnitOfWorkId uow, CancellationToken ct)
        {
            var list = Mapper.Map<IList<RestaurantDto>>(await RestaurantRepository.GetFilteredRestaurantsAsync(filter, uow, ct));

            foreach(var rest in list)
            {
                rest.Status = await OpeningRepository.GetRestaurantStatusAsync(rest.Id, uow, ct);
            }

            return list;
        }

        public async Task<int> GetFilteredRestaurantsCountAsync(RestaurantFilter filter, UnitOfWorkId uow, CancellationToken ct)
        {
            return await RestaurantRepository.GetFilteredRestaurantsCountAsync(filter, uow, ct);
        }

        public async Task<RestaurantDetailDto> GetRestaurantDetail(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var restaurant = Mapper.Map<RestaurantDetailDto>(await RestaurantRepository.GetRestaurantDetail(id, uow, ct));

            var openings = await OpeningRepository.GetRestaurantOpeningHours(id, uow, ct);
            var openingList = new List<string>();

            restaurant.Status = await OpeningRepository.GetRestaurantStatusAsync(id, uow, ct);

            foreach (var open in openings)
            {
                var text = $"{await OpeningRepository.GetDayNameByIdAsync(open.DayId, uow, ct)}: {open.From.ToString("HH:mm")} - {open.To.ToString("HH:mm")}";
                openingList.Add(text);
            }

            restaurant.Opening = openingList;

            return restaurant;
        }

        public async Task<IList<RestaurantDto>> GetRestaurantsByOrganization(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var list = Mapper.Map<IList<RestaurantDto>>(await RestaurantRepository.GetRestaurantsByOrganization(id, uow, ct));

            foreach (var rest in list)
            {
                rest.Status = await OpeningRepository.GetRestaurantStatusAsync(rest.Id, uow, ct);
            }

            return list;
        }

        public async Task UpdateRestaurantAsync(UpdateRestaurantDto updateRestaurantDto, Guid loggedUserId, UnitOfWorkId uow, CancellationToken ct)
        {
            var user = await UserRepository.GetByIdAsync(loggedUserId, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, loggedUserId);
            if (await RestaurantRepository.CanManipulate(uow, ct, user.Id, updateRestaurantDto.Id))
            {
                await RestaurantRepository.UpdateAsync(uow, ct, updateRestaurantDto);
            }
            else throw new UnauthorizedAccessException();
        }
    }
}
