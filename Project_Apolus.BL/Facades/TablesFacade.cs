﻿using AutoMapper;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Exceptions;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Tables;
using Project_Apolus.Common.Params;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public class TablesFacade : ITablesFacade
    {

        private readonly ITableRepository TablesRepository;
        private readonly IUserRepository UserRepository;
        private readonly IDateTimeProvider DateTimeProvider;
        private readonly IMapper Mapper;

        public TablesFacade(ITableRepository tableRepository, IDateTimeProvider dateTimeProvider, IMapper mapper, IUserRepository userRepository)
        {
            TablesRepository = tableRepository;
            UserRepository = userRepository;
            DateTimeProvider = dateTimeProvider;
            Mapper = mapper;
        }

        public async Task CreateTableAsync(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId, CreateTableDto table, Guid userId)
        {
            var user = await UserRepository.GetByIdAsync(userId, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, userId);
            if (await TablesRepository.CanManipulate(userId, restaurantId, uow, ct))
            {
                var tableModel = new Table(restaurantId, table.Name);
                await TablesRepository.CreateAsync(tableModel, uow, ct);
            }
            else throw new UnauthorizedAccessException();
        }

        public async Task DeleteTableAsync(UnitOfWorkId uow, CancellationToken ct, Guid tableId, Guid userId)
        {
            var user = await UserRepository.GetByIdAsync(userId, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, userId);
            if (await TablesRepository.CanDelete(userId, tableId, uow, ct)) await TablesRepository.DeleteByIdAsync(tableId, uow, ct);
            else throw new UnauthorizedAccessException();
        }

        public async Task<IList<TableDto>> GetAllTablesAsync(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId, Guid userId)
        {
            var user = await UserRepository.GetByIdAsync(userId, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, userId);
            if (await TablesRepository.CanManipulate(userId, restaurantId, uow, ct)) 
                return Mapper.Map<IList<TableDto>>(await TablesRepository.GetByRestaurantAsync(restaurantId, uow, ct));
            else throw new UnauthorizedAccessException();
        }

        public async Task<IList<TableDto>> GetAvailableTablesAsync(UnitOfWorkId uow, CancellationToken ct, Guid id, TablesQuery param)
        {
            return Mapper.Map<IList<TableDto>>(await TablesRepository.GetAvailableTablesAsync(uow, ct, id, param));
        }
    }
}
