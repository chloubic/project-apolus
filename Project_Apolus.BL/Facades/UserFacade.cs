﻿using AutoMapper;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Enums;
using Project_Apolus.Common.Exceptions;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Facades
{
    public class UserFacade : IUserFacade
    {

        private readonly IUserRepository UserRepository;

        private readonly IOrganizationRepository OrganizationRepository;

        private readonly IDateTimeProvider DateTimeProvider;

        private readonly IMapper Mapper;

        public UserFacade(IUserRepository userRepository, IDateTimeProvider dateTimeProvider, IOrganizationRepository organizationRepository, IMapper mapper)
        {
            UserRepository = userRepository;
            DateTimeProvider = dateTimeProvider;
            OrganizationRepository = organizationRepository;
            Mapper = mapper;
        }

        public async Task CreateClient(CreateClientDto createClientDto, UnitOfWorkId uow, CancellationToken ct)
        {
            var clientOrgId = await OrganizationRepository.GetClientOrganizationGuid(uow, ct);
            var user = new User(
                Guid.Empty,
                createClientDto.Name,
                clientOrgId,
                DateTimeProvider.Now,
                createClientDto.Name.Split(" ")[0],
                createClientDto.Email,
                createClientDto.Phone
            );
            await UserRepository.CreateWithPasswordAsync(user, createClientDto.Password, new string[] { Roles.Client }, uow);
        }

        public async Task CreateAdmin(CreateAdminDto createAdminDto, UnitOfWorkId uow, CancellationToken ct)
        {
            var organization = new Organization(
                Guid.NewGuid(),
                createAdminDto.OrganizationName,
                DateTimeProvider.Now,
                DateTimeProvider.Now,
                false
            );

            var user = new User(
                Guid.Empty,
                createAdminDto.Name,
                organization.Id,
                DateTimeProvider.Now,
                createAdminDto.Name.Split(" ")[0],
                createAdminDto.Email,
                createAdminDto.Phone
            );
            await UserRepository.CreateWithPasswordAndOrganizationAsync(user, createAdminDto.Password, new string[] { Roles.Admin }, uow, organization);
        }

        public async Task CreateOperator(CreateClientDto createOperatorDto, UnitOfWorkId uow, CancellationToken ct, Guid id)
        {
            var user = await UserRepository.GetByIdAsync(id, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, id);
            var organization = await OrganizationRepository.GetByIdAsync(user.OrganizationId, uow, ct);
            if (organization == null) throw new ItemNotFoundException(Constants.ORGANIZATION_ITEM_NAME, user.OrganizationId);
            var userOperator = new User(
                Guid.Empty,
                createOperatorDto.Name,
                organization.Id,
                DateTimeProvider.Now,
                createOperatorDto.Name.Split(" ")[0],
                createOperatorDto.Email,
                createOperatorDto.Phone
            );
            await UserRepository.CreateWithPasswordAsync(userOperator, createOperatorDto.Password, new string[] { Roles.Operator }, uow);
        }

        public async Task RemoveUserAsync(Guid id, Guid loggedUserId, UnitOfWorkId uow, CancellationToken ct)
        {
            var user = await UserRepository.GetByIdAsync(id, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, id);
            var loggedUser = await UserRepository.GetByIdAsync(loggedUserId, uow, ct);
            if (loggedUser == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, loggedUserId);
            if (await UserRepository.CheckRoleAsync(user, Roles.Client) && id.Equals(loggedUserId)) await UserRepository.DeleteClientAsync(id, uow);
            else if (await UserRepository.CheckRoleAsync(user, Roles.Operator) && 
                (id.Equals(loggedUserId) || user.OrganizationId.Equals(loggedUser.OrganizationId))) await UserRepository.DeleteAsync(id, uow);
            else if (await UserRepository.CheckRoleAsync(user, Roles.Admin) && id.Equals(loggedUserId)) await UserRepository.DeleteAdminAsync(id, uow);
        }

        public async Task<UserDto> GetUserInfo(UnitOfWorkId uow, CancellationToken ct, Guid id)
        {
            var user = await UserRepository.GetByIdAsync(id, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, id);
            return Mapper.Map<UserDto>(user);
        }

        public async Task UpdateUserInfoAsync(UnitOfWorkId uow, CancellationToken ct, UserDto user, Guid id)
        {
            await UserRepository.UpdateUserInfoAsync(uow, ct, user, id);
        }

        public async Task<IList<OperatorDto>> GetOperatorsAsync(UnitOfWorkId uow, CancellationToken ct, Guid id)
        {
            var user = await UserRepository.GetByIdAsync(id, uow, ct);
            if (user == null) throw new ItemNotFoundException(Constants.USER_ITEM_NAME, id);
            return Mapper.Map<IList<OperatorDto>>(await UserRepository.GetOperatorsAsync(uow, ct, user.OrganizationId));
        }
    }
}
