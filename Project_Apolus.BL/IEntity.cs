﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.BL
{
    public interface IEntity<TKey>
    {
        TKey Id { get; set; }
    }
}
