﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.BL
{
    public interface IStorage
    {
        Task<Byte[]> GetAsync(string filename);
        Task UploadAsync(string filename, Stream formFile);
    }
}
