﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Logger
{
    public interface ILogger
    {

        public void Log(string message);

        Task<Byte[]> GetAsync();

    }
}
