﻿using Microsoft.AspNetCore.Hosting;
using Project_Apolus.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Logger
{
    public class Logger : ILogger
    {

        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly string logPath;

        public Logger(IHostingEnvironment environment)
        {
            this._hostingEnvironment = environment;
            var uploads = Path.Combine(_hostingEnvironment.ContentRootPath, "uploads");
            this.logPath = Path.Combine(uploads, "Log.txt");
            if (!File.Exists(this.logPath))
            {
                var file = File.Create(logPath);
                file.Close();
            }
        }

        public async Task<Byte[]> GetAsync()
        {
            if (!File.Exists(this.logPath)) throw new ItemNotFoundException("Log file", Guid.Empty);
            Byte[] b = File.ReadAllBytes(this.logPath);
            return b;
        }

        public void Log(string message)
        {
            File.AppendAllText(this.logPath, message + Environment.NewLine);
        }
    }
}
