﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.BL.Model
{
    public interface IDomainObject<out TKey>
    {
        TKey Id { get; }
    }
}
