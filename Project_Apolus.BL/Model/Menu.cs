﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.BL.Model
{
    public class Menu : IDomainObject<Guid>
    {
        public Guid Id { get; }

        public Guid RestaurantId { get; }

        public int Order { get; }
        public string Name { get; }

        public Menu(Guid restaurantId, string name, int order) : this(Guid.Empty, restaurantId, name, order) { }

        public Menu(Guid id, Guid restaurantId, string name, int order)
        {
            Id = id;
            RestaurantId = restaurantId;
            Name = name;
            Order = order;
        }
    }
}
