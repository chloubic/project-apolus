﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.BL.Model
{
    public class MenuItem : IDomainObject<Guid>
    {
        public Guid Id { get; }
        public Guid MenuId { get; }
        public string Name { get; }
        public string Description { get; }
        public string Weight { get; }
        public string Price { get; }
        public int Order { get; }

        public MenuItem(Guid menuId, string name, string description, string weight, string price, int order) 
            : this(Guid.Empty, menuId, name, description, weight, price, order) { }

        public MenuItem(Guid id, Guid menuId, string name, string description, string weight, string price, int order)
        {
            Id = id;
            MenuId = menuId;
            Name = name;
            Description = description;
            Weight = weight;
            Price = price;
            Order = order;
        }
    }
}
