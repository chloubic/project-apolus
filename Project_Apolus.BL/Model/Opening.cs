﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.BL.Model
{
    public class Opening : IDomainObject<Guid>
    {
        public Guid Id { get; set; }

        public Guid DayId { get; set; }

        public Guid RestaurantId { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public Opening(Guid id, Guid dayId, Guid restaurantId, DateTime from, DateTime to)
        {
            Id = id;
            DayId = dayId;
            RestaurantId = restaurantId;
            From = from;
            To = to;
        }
    }
}
