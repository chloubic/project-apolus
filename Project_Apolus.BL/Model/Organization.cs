﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.BL.Model
{
    public class Organization : IDomainObject<Guid>
    {
        public Guid Id { get; }
        public string Name { get; private set; }
        public DateTime CreatedDate { get; }
        public DateTime UpdatedDate { get; private set; }
        public bool IsClientsOrg { get; }

        public void SetName(string name) => Name = name;
        public void SetUpdatedDate(DateTime updatedDate) => UpdatedDate = updatedDate;

        public Organization(Guid id, string name, DateTime createdDate, DateTime updatedDate, bool isClientsOrg)
        {
            Id = id;
            Name = name;
            CreatedDate = createdDate;
            UpdatedDate = updatedDate;
            IsClientsOrg = isClientsOrg;
        }
    }
}
