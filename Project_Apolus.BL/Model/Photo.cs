﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.BL.Model
{
    public class Photo : IDomainObject<Guid>
    {
        public Guid Id { get; }

        public Guid RestaurantId { get; }

        public string Url { get; }

        public Photo(Guid id, Guid restaurantId, string url)
        {
            Id = id;
            RestaurantId = restaurantId;
            Url = url;
        }
    }
}
