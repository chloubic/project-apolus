﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.BL.Model
{
    public class Reservation : IDomainObject<Guid>
    {
        public Guid Id { get; set; }

        public Guid TableId { get; set; }

        public Guid UserId { get; set; }

        public DateTime Date { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }
    }
}
