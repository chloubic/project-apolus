﻿using Project_Apolus.Common.Model.Photogallery;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.BL.Model
{
    public class Restaurant : IDomainObject<Guid>
    {

        public Guid Id { get; }

        public string Name { get; private set; }

        public string Description { get; private set; }

        public string Address { get; private set; }

        public string MainImage { get; private set; }

        public IList<Photo> Gallery { get; }

        public Guid OrganizationId { get; private set; }

        public DateTime CreatedDate { get; }

        public DateTime UpdatedDate { get; private set; }

        public void SetOrganizationId(Guid organizationId) => OrganizationId = organizationId;

        public Restaurant(string name, string description, string address, string mainImage, Guid organizationId)
            : this(Guid.Empty, name, description, address, mainImage, new List<Photo>(), organizationId, DateTime.Now, DateTime.Now) { }

        public Restaurant(string name, string description, string address, string mainImage)
            : this(Guid.Empty, name, description, address, mainImage, new List<Photo>(), Guid.Empty, DateTime.Now, DateTime.Now) { }

        public Restaurant(Guid id, string name, string description, string address, string mainImage, Guid organizationId)
            : this(id, name, description, address, mainImage, new List<Photo>(), organizationId, DateTime.Now, DateTime.Now) { }

        public Restaurant(Guid id, string name, string description, string address, string mainImage, Guid organizationId, DateTime createdDate, DateTime updatedDate)
            : this(id, name, description, address, mainImage, new List<Photo>(), organizationId, createdDate, updatedDate) { }

        public Restaurant(Guid id, string name, string description, string address, string mainImage, DateTime createdDate, DateTime updatedDate)
            : this(id, name, description, address, mainImage, new List<Photo>(), Guid.Empty, createdDate, updatedDate) { }

        public Restaurant(Guid id, string name, string description, string address, string mainImage, IList<Photo> gallery, Guid organizationId, DateTime createdDate, DateTime updatedDate)
        {
            Id = id;
            Name = name;
            Description = description;
            Address = address;
            MainImage = mainImage;
            Gallery = gallery;
            OrganizationId = organizationId;
            CreatedDate = createdDate;
            UpdatedDate = updatedDate;
        }
    }
}
