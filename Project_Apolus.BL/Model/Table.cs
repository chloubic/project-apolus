﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.BL.Model
{
    public class Table : IDomainObject<Guid>
    {
        public Guid Id { get; set; }

        public Guid RestaurantId { get; set; }

        public string Name { get; set; } = null!;

        public Table(Guid restaurantId, string name) : this(Guid.Empty, restaurantId, name) { }

        public Table(Guid id, Guid restaurantId, string name)
        {
            Id = id;
            RestaurantId = restaurantId;
            Name = name;
        }

    }
}
