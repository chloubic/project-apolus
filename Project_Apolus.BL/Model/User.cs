﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.BL.Model
{
    public class User : IDomainObject<Guid>
    {
        public Guid Id { get; }
        public string Name { get; private set; }
        public Guid OrganizationId { get; private set; }
        public DateTime CreatedDate { get; }
        public string UserName { get; }
        public string Email { get; private set; }
        public string PhoneNumber { get; private set; }
        public string RecoveryToken { get; private set; }
        public DateTime RecoveryTokenValidity { get; private set; }

        public void SetName(string name) { if (!string.IsNullOrEmpty(name)) Name = name; }
        public void SetOrganizationId(Guid id) { if (!id.Equals(Guid.Empty)) OrganizationId = id; }
        public void SetEmail(string email) { if (!string.IsNullOrEmpty(email)) Email = email; }
        public void SetPhoneNumber(string phone) => PhoneNumber = phone;
        public void SetRecoveryToken(string token) => RecoveryToken = token;
        public void SetRecoveryTokenValidity(DateTime validity) => RecoveryTokenValidity = validity;

        public User(Guid id, string name, Guid organizationId, DateTime createdDate, string userName, string email, string phoneNumber)
        : this(id, name, organizationId, createdDate, userName, email, phoneNumber, string.Empty, DateTime.Now) { }

        public User(Guid id, string name, Guid organizationId, DateTime createdDate, string userName, string email, string phoneNumber,
            string recoveryToken, DateTime recoveryTokenValidity)
        {
            Id = id;
            Name = name;
            CreatedDate = createdDate;
            UserName = userName;
            Email = email;
            PhoneNumber = phoneNumber;
            OrganizationId = organizationId;
            RecoveryToken = recoveryToken;
            RecoveryTokenValidity = recoveryTokenValidity;
        }

        /// <summary>
        /// Creates new object - doesn't exist in the persisted store.
        /// </summary>
        public static User CreateNew(string name, Guid organizationId, DateTime createdDate, string userName, string email, string phoneNumber)
        {
            var id = Guid.NewGuid();
            return new User(id, name, organizationId, createdDate, userName, email, phoneNumber);
        }
    }
}
