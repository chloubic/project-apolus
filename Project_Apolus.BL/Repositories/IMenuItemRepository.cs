﻿using Project_Apolus.BL.Model;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Menu;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Repositories
{
    public interface IMenuItemRepository : IRepository<MenuItem, Guid>
    {
        Task<bool> HasRightsToCreateMenuItems(UnitOfWorkId uow, Guid loggedUser, Guid menuId);
        Task<bool> HasRightsToUpdateMenuItems(UnitOfWorkId uow, Guid id1, Guid id2);
        Task UpdateMenuItemAsync(UpdateMenuItemDto items, UnitOfWorkId uow, CancellationToken ct);
        Task DeleteMenuItemAsync(UnitOfWorkId uow, CancellationToken ct, Guid itemId);
        Task ChangeMenuItemsOrderAsync(UnitOfWorkId uow, CancellationToken ct, Guid menuId, IList<ItemOrderUpdateDto> items);
    }
}
