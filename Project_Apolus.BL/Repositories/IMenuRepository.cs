﻿using Project_Apolus.BL.Model;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Menu;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Repositories
{
    public interface IMenuRepository : IRepository<Menu, Guid>
    {
        Task<ICollection<Menu>> GetMenuCategoriesByRestaurantId(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId);
        Task<ICollection<MenuItem>> GetCategoryItemsByCategoryId(UnitOfWorkId uow, CancellationToken ct, Guid categoryId);
        Task<bool> HasRightsToCreateMenu(UnitOfWorkId uow, Guid loggedUser, Guid restaurantId);
        Task<bool> HasRightsToUpdateMenu(UnitOfWorkId uow, Guid id1, Guid id2);
        Task UpdateMenuAsync(UpdateMenuDto categories, UnitOfWorkId uow, CancellationToken ct);
        Task DeleteMenuAsync(UnitOfWorkId uow, CancellationToken ct, Guid categoryId);
        Task ChangeMenuCategoryOrderAsync(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId, IList<ItemOrderUpdateDto> items);
    }
}
