﻿using Project_Apolus.BL.Model;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Opening;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Repositories
{
    public interface IOpeningRepository : IRepository<Opening, Guid>
    {
        Task EraseRestaurantOpeningHours(Guid id, UnitOfWorkId uow, CancellationToken ct);
        Task UpdateRestaurantOpeningHours(List<Opening> openingList, UnitOfWorkId uow, CancellationToken ct);
        Task<IList<Opening>> GetRestaurantOpeningHours(Guid id, UnitOfWorkId uow, CancellationToken ct);
        Task<string> GetDayNameByIdAsync(Guid id, UnitOfWorkId uow, CancellationToken ct);
        Task<bool> GetRestaurantStatusAsync(Guid id, UnitOfWorkId uow, CancellationToken ct);
        Task<IList<DayDto>> GetDays(UnitOfWorkId uow, CancellationToken ct);
    }
}
