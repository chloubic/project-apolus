﻿using Project_Apolus.BL.Model;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Organization;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Repositories
{
    public interface IOrganizationRepository : IRepository<Organization, Guid>
    {

        Task CreateAsync(Organization organization, UnitOfWorkId uow, CancellationToken ct);
        Task<Guid> GetClientOrganizationGuid(UnitOfWorkId uow, CancellationToken ct);
        Task UpdateOrganization(UnitOfWorkId uow, CancellationToken ct, UpdateOrganizationDto update, Guid orgId);

    }
}
