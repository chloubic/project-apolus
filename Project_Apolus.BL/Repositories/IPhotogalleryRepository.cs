﻿using Project_Apolus.BL.Model;
using Project_Apolus.Common.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Repositories
{
    public interface IPhotogalleryRepository : IRepository<Photo, Guid>
    {
        Task EraseGalleryAsync(Guid id, UnitOfWorkId uow, CancellationToken ct);
        Task UpdateGalleryAsync(List<Photo> photoList, UnitOfWorkId uow, CancellationToken ct);
        Task<IList<Photo>> GetRestaurantPhotos(Guid id, UnitOfWorkId uow, CancellationToken ct);
    }
}
