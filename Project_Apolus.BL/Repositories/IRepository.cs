﻿using Project_Apolus.BL.Model;
using Project_Apolus.Common.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Repositories
{
    public interface IRepository<TDomainObject, in TKey>
        where TDomainObject : class, IDomainObject<TKey>
    {
        void Delete(TDomainObject domainObject, UnitOfWorkId uow);

        void Delete(IEnumerable<TDomainObject> domainObjects, UnitOfWorkId uow);

        void Delete(TKey id, UnitOfWorkId uow);

        void Delete(IEnumerable<TKey> ids, UnitOfWorkId uow);

        Task<TDomainObject> GetByIdAsync(TKey id, UnitOfWorkId uow, CancellationToken ct);
        Task<IList<TDomainObject>> GetByIdsAsync(IEnumerable<TKey> ids, UnitOfWorkId uow, CancellationToken ct);
        Task InsertAsync(TDomainObject domainObject, UnitOfWorkId uow, CancellationToken ct);

        Task InsertAsync(IEnumerable<TDomainObject> domainObjects, UnitOfWorkId uow, CancellationToken ct);

        void Update(TDomainObject domainObject, UnitOfWorkId uow);

        void Update(IEnumerable<TDomainObject> domainObjects, UnitOfWorkId uow);
        Task<List<TDomainObject>> GetAllAsync(UnitOfWorkId uow, CancellationToken ct);
        Task<int> GetCountAsync(UnitOfWorkId uow, CancellationToken ct);
    }
}
