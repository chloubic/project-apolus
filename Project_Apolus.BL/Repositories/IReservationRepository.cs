﻿using Project_Apolus.BL.Model;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Reservations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Repositories
{
    public interface IReservationRepository : IRepository<Reservation, Guid>
    {
        Task DeleteAsync(UnitOfWorkId uow, CancellationToken ct, Guid reservationId, Guid userId);
        Task CreateAsync(UnitOfWorkId uow, CancellationToken ct, Reservation reservation);
        Task<IList<ReservationDto>> GetMyReservationsAsync(UnitOfWorkId uow, CancellationToken ct, Guid id);
        Task<IList<ReservationDto>> GetRestaurantReservationAsync(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId);
        Task<bool> CanDelete(Guid userId, Guid reservationId, UnitOfWorkId uow, CancellationToken ct);
        Task<bool> CanReservate(Guid tableId, DateTime date, DateTime from, DateTime to, UnitOfWorkId uow, CancellationToken ct);
    }
}
