﻿using Project_Apolus.BL.Model;
using Project_Apolus.Common.Filters;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Restaurant;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Repositories
{
    public interface IRestaurantRepository : IRepository<Restaurant, Guid>
    {
        Task CreateAsync(Restaurant restaurant, UnitOfWorkId uow, CancellationToken ct);
        Task<IList<Restaurant>> GetFilteredRestaurantsAsync(RestaurantFilter filter, UnitOfWorkId uow, CancellationToken ct);
        Task<IList<Restaurant>> GetRestaurantsByOrganization(Guid id, UnitOfWorkId uow, CancellationToken ct);
        Task<Restaurant> GetRestaurantDetail(Guid id, UnitOfWorkId uow, CancellationToken ct);
        Task DeleteAsync(Guid id, UnitOfWorkId uow, CancellationToken ct);
        Task<int> GetFilteredRestaurantsCountAsync(RestaurantFilter filter, UnitOfWorkId uow, CancellationToken ct);
        Task UpdateAsync(UnitOfWorkId uow, CancellationToken ct, UpdateRestaurantDto updateRestaurantDto);
        Task<bool> CanManipulate(UnitOfWorkId uow, CancellationToken ct, Guid id, Guid restaurantId);
    }
}
