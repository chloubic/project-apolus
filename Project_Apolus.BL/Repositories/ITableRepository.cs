﻿using Project_Apolus.BL.Model;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Params;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Repositories
{
    public interface ITableRepository : IRepository<Table, Guid>
    {
        Task<bool> CanDelete(Guid userId, Guid tableId, UnitOfWorkId uow, CancellationToken ct);
        Task DeleteByIdAsync(Guid tableId, UnitOfWorkId uow, CancellationToken ct);
        Task<bool> CanManipulate(Guid userId, Guid restaurantId, UnitOfWorkId uow, CancellationToken ct);
        Task<IList<Table>> GetByRestaurantAsync(Guid restaurantId, UnitOfWorkId uow, CancellationToken ct);
        Task CreateAsync(Table tableModel, UnitOfWorkId uow, CancellationToken ct);
        Task<IList<Table>> GetAvailableTablesAsync(UnitOfWorkId uow, CancellationToken ct, Guid id, TablesQuery param);
    }
}
