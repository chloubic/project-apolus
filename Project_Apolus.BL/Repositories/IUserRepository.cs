﻿using Project_Apolus.BL.Model;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Auth;
using Project_Apolus.Common.Model.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.BL.Repositories
{
    public interface IUserRepository : IRepository<User, Guid>
    {
        Task ChangePasswordAsync(ChangePasswordDto dto, Guid userId);
        Task ChangeEmailAsync(Guid userId, string newEmail);
        Task CreateWithPasswordAsync(User user, string password, string[] roles, UnitOfWorkId uow);
        Task CreateWithPasswordAndOrganizationAsync(User user, string password, string[] roles, UnitOfWorkId uow, Organization organization);
        Task SetPasswordAsync(User user, string password);
        Task UpdateUserAsync(UpdateUserSettingsDto user, UnitOfWorkId uow);
        Task<bool> CheckRoleAsync(User user, string role);
        Task ChangeRoleAsync(Guid id, string role, UnitOfWorkId uow);
        Task<bool> UserExistsAsync(Guid id, UnitOfWorkId uow, CancellationToken ct);
        Task DeleteAsync(Guid id, UnitOfWorkId uow);
        Task<User> GetByEmailAsync(UnitOfWorkId uow, CancellationToken ct, string email);
        Task<User> GetUserByRecoveryToken(UnitOfWorkId uow, CancellationToken ct, string token);
        Task<string> GetUserRoleAsync(UnitOfWorkId uow, CancellationToken ct, Guid id);
        Task<bool> UpdateUserTokenAsync(UnitOfWorkId uow, CancellationToken ct, string email, Guid token);
        Task SetPasswordByTokenAsync(string token, string password, UnitOfWorkId uow, CancellationToken ct);
        Task DeleteClientAsync(Guid id, UnitOfWorkId uow);
        Task DeleteAdminAsync(Guid id, UnitOfWorkId uow);
        Task UpdateUserInfoAsync(UnitOfWorkId uow, CancellationToken ct, UserDto user, Guid id);
        Task<IList<User>> GetOperatorsAsync(UnitOfWorkId uow, CancellationToken ct, Guid organizationId);
    }
}
