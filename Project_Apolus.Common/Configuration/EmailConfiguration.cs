﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Configuration
{
    public class EmailConfiguration
    {
        public string ApiKey { get; set; } = null!;
        public string AdminEmail { get; set; } = null!;
        public string BaseURL { get; set; } = null!;
    }
}
