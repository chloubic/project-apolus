﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Enums
{
    public class OrderStates
    {

        public const string ORDERED = "Ordered";
        public const string PREPARING = "Preparing";
        public const string COMPLETED = "Completed";

    }
}
