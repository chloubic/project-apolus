﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Enums
{
    public class PaymentStates
    {

        public const string NOT_PAID = "Not paid";
        public const string CASH = "Cash";
        public const string CARD = "Card";
        public const string GOPAY = "GoPay";

    }
}
