﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Exceptions
{
    public class BusinessException : Exception
    {
        public BusinessException(string message) : base(message)
        {
        }

        public BusinessException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public static BusinessException CreateProjectedIdIsDifferent(Guid domainObjectId, Guid efEntityId)
        {
            return new BusinessException($"ID of domain object ({domainObjectId}) is different from EF entity ID ({efEntityId}).");
        }
    }
}
