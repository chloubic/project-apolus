﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Exceptions
{
    public class BusinessRuleException : BusinessException
    {
        public string LocalizedRuleErrorMessage { get; }

        public BusinessRuleException(string localizedRuleErrorMessage)
            : base($"Business rule violated: \"{localizedRuleErrorMessage}\"")
        {
            LocalizedRuleErrorMessage = localizedRuleErrorMessage;
        }

        public BusinessRuleException(string localizedRuleErrorMessage, Exception innerException)
            : base($"Business rule violated: \"{localizedRuleErrorMessage}\"", innerException)
        {
            LocalizedRuleErrorMessage = localizedRuleErrorMessage;
        }
    }
}
