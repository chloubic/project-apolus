﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Exceptions
{
    public class ItemNotFoundException : BusinessException
    {
        public string ItemTypeName { get; }
        public Guid ItemId { get; }

        public ItemNotFoundException(string itemTypeName, Guid itemId)
            : base($"Item {itemTypeName} Id {itemId} was not found when executing business operation.")
        {
            ItemTypeName = itemTypeName;
            ItemId = itemId;
        }

        public ItemNotFoundException(string itemTypeName, Guid itemId, Exception innerException)
            : base($"Item {itemTypeName} Id {itemId} was not found when executing business operation.", innerException)
        {
            ItemTypeName = itemTypeName;
            ItemId = itemId;
        }
    }
}
