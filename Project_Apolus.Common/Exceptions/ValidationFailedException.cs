﻿using Project_Apolus.Common.ResponseObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Exceptions
{
    public class ValidationFailedException : BusinessException
    {
        public ValidationResult ValidationResult { get; set; }

        public ValidationFailedException(ValidationResult validationResult) : base($"Validation failed. See {nameof(ValidationResult)} for details.")
        {
            ValidationResult = validationResult;
        }

        public ValidationFailedException(Exception innerException, ValidationResult validationResult) : base($"Validation failed. See {nameof(ValidationResult)} for details.", innerException)
        {
            ValidationResult = validationResult;
        }
    }
}
