﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Filters
{
    public class RestaurantFilter
    {

        public string Name { get; }

        public int Page { get; }

        public int Limit { get; }

        public bool DescOrder { get; }

        public RestaurantFilter(string name, int page, int limit, bool descOrder)
        {
            Name = name;
            Page = page;
            Limit = limit;
            DescOrder = descOrder;
        }

    }
}
