﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Apolus.Common.Infrastructure
{
    public static class JsonSerializationConfiguration
    {
        public static JsonSerializerSettings Default
        {
            get
            {
                var serializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new DefaultContractResolver
                    {
                        NamingStrategy = new CamelCaseNamingStrategy()
                    }
                };

                Configure(serializerSettings);
                return serializerSettings;
            }
        }

        public static void Configure(JsonSerializerSettings serializerSettings)
        {
            SetIgnoreNullValueHandling(serializerSettings);
            SetEnumConverterWithDefaultNamingStrategy(serializerSettings);
            SetTypeNameHandling(serializerSettings);
        }

        public static void SetIgnoreNullValueHandling(JsonSerializerSettings serializerSettings)
        {
            serializerSettings.NullValueHandling = NullValueHandling.Ignore;
        }

        public static void SetEnumConverterWithDefaultNamingStrategy(JsonSerializerSettings serializerSettings)
        {
            var stringEnumConverter = serializerSettings.Converters.OfType<StringEnumConverter>().FirstOrDefault();
            if (stringEnumConverter == null)
            {
                serializerSettings.Converters.Add(new StringEnumConverter
                {
                    NamingStrategy = new DefaultNamingStrategy(),
                    AllowIntegerValues = true
                });
            }

            else
            {
                stringEnumConverter.NamingStrategy = new DefaultNamingStrategy();
                stringEnumConverter.AllowIntegerValues = true;
            }
        }

        private static void SetTypeNameHandling(JsonSerializerSettings serializerSettings)
        {
            serializerSettings.TypeNameHandling = TypeNameHandling.Auto;
        }
    }
}
