﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Infrastructure
{
    public class UnitOfWorkId : IDisposable
    {
        public event Action<Guid>? Disposing;

        public Guid Id { get; }

        public UnitOfWorkId(Guid id)
        {
            Id = id;
        }

        public void Dispose()
        {
            Disposing?.Invoke(Id);
        }
    }
}
