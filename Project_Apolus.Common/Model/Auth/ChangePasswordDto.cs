﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Auth
{
    public class ChangePasswordDto
    {
        public string OldPassword { get; }

        public string NewPassword { get; }

        public ChangePasswordDto(string oldPassword, string newPassword)
        {
            OldPassword = oldPassword;
            NewPassword = newPassword;
        }
    }
}
