﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Project_Apolus.Common.Model.Auth
{
    public class LoginDto
    {
        [EmailAddress]
        [Required]
        public string Email { get; }

        [Required]
        public string Password { get; }

        public LoginDto(string email, string password)
        {
            Email = email;
            Password = password;
        }
    }
}
