﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Auth
{
    public class LoginResultDto
    {
        public string Token { get; }

        public LoginResultDto(string token)
        {
            Token = token;
        }
    }
}
