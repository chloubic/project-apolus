﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model
{
    public class LoggedUser
    {
        public Guid Id { get; }

        public string UserName { get; }

        public string Email { get; }
        public ICollection<string> Roles { get; }

        //public Guid? OrganizationId { get; }

        public LoggedUser(Guid id, string userName, string email, ICollection<string> roles, Guid? organizationId)
        {
            Id = id;
            //OrganizationId = organizationId;
            UserName = userName;
            Email = email;
            Roles = roles;
        }

        public bool IsInRole(string roleName)
        {
            return Roles.Contains(roleName);
        }
    }
}
