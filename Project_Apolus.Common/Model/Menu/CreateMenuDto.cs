﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Menu
{
    public class CreateMenuDto
    {

        public string Name { get; private set; } = null!;

        public int Order { get; private set; }

        public CreateMenuDto(string name, int order)
        {
            Name = name;
            Order = order;
        }

    }
}
