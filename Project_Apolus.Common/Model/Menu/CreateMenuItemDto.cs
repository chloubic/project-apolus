﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Menu
{
    public class CreateMenuItemDto
    {

        public string Name { get; private set; }

        public string Description { get; private set; }

        public string Weight { get; private set; }

        public string Price { get; private set; }

        public int Order { get; private set; }

        public CreateMenuItemDto(string name, string description, string weight, string price, int order)
        {
            Name = name;
            Description = description;
            Weight = weight;
            Price = price;
            Order = order;
        }

    }
}
