﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Menu
{
    public class ItemOrderUpdateDto
    {

        public Guid Id { get; set; }

        public ItemOrderUpdateDto(Guid id)
        {
            Id = id;
        }

    }
}
