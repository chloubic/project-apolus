﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Menu
{
    public class MenuCategoryDto
    {

        public Guid Id { get; private set; }

        public string Name { get; private set; } = null!;

        public int Order { get; private set; }

        public MenuCategoryDto(Guid id, string name, int order)
        {
            Id = id;
            Name = name;
            Order = order;
        }

    }
}
