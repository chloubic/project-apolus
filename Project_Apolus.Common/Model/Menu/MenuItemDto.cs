﻿using Microsoft.Extensions.WebEncoders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Menu
{
    public class MenuItemDto
    {

        public Guid Id { get; private set; }

        public string Name { get; private set; } = null!;

        public string Description { get; private set; } = null!;

        public string Weight { get; private set; }

        public string Price { get; private set; }

        public int Order { get; private set; }

        public MenuItemDto(Guid id, string name, string description, string weight, string price, int order)
        {
            Id = id;
            Name = name;
            Description = description;
            Weight = weight;
            Price = price;
            Order = order;
        }

    }
}
