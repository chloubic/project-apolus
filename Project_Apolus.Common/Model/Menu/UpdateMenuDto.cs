﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Menu
{
    public class UpdateMenuDto
    {
        public Guid Id { get; private set; }

        public string Name { get; private set; }

        public UpdateMenuDto(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
