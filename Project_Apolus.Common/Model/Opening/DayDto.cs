﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Opening
{
    public class DayDto
    {

        public Guid Id { get; set; }

        public string Name { get; set; }

        public DayDto(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

    }
}
