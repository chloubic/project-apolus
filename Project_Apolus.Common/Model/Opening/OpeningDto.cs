﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Opening
{
    public class OpeningDto
    {
        public Guid DayId { get; }

        public DateTime From { get; }

        public DateTime To { get; }

        public OpeningDto(Guid dayId, DateTime from, DateTime to)
        {
            DayId = dayId;
            From = from;
            To = to;
        }
    }
}
