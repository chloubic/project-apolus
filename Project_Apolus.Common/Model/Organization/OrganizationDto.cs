﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Organization
{
    public class OrganizationDto
    {

        public Guid Id { get; }

        public string Name { get; }

        public OrganizationDto(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
