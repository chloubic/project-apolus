﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Organization
{
    public class UpdateOrganizationDto
    {
        public string Name { get; }

        public UpdateOrganizationDto(Guid id, string name)
        {
            Name = name;
        }

    }
}
