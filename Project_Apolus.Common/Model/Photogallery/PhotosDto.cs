﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Photogallery
{
    public class PhotosDto
    {

        public IList<string> Urls { get; }

        public PhotosDto(IList<string> urls)
        {
            Urls = urls;
        }
    }
}
