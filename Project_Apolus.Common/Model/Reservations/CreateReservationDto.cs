﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Reservations
{
    public class CreateReservationDto
    {

        public DateTime Date { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }


    }
}
