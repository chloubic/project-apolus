﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Reservations
{
    public class ReservationDto
    {

        public Guid Id { get; set; }

        public string UserName { get; set; } = null!;

        public string UserEmail { get; set; } = null!;

        public string UserPhone { get; set; } = null!;

        public string Table { get; set; } = null!;

        public string Restaurant { get; set; } = null!;

        public DateTime Date { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

    }
}
