﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Restaurant
{
    public class CreateRestaurantDto
    {
        public string Name { get; set; } = null!;

        public string Description { get; set; } = null!;

        public string Address { get; set; } = null!;

        public string MainImage { get; set; } = null!;

        public CreateRestaurantDto(string name, string description, string address, string mainImage)
        {
            Name = name;
            Description = description;
            Address = address;
            MainImage = mainImage;
        }
    }
}
