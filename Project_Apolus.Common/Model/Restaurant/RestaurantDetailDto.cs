﻿using Project_Apolus.Common.Model.Opening;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Restaurant
{
    public class RestaurantDetailDto
    {

        public Guid Id { get; set; }

        public string Name { get; set; } = null!;

        public string Description { get; set; } = null!;

        public string Address { get; set; } = null!;

        public string MainImage { get; set; } = null!;

        public bool Status { get; set; }

        public IList<string> Opening { get; set; }

        public RestaurantDetailDto(Guid id, string name, string description, string address, string mainImage)
        {
            Id = id;
            Name = name;
            Description = description;
            Address = address;
            MainImage = mainImage;
            Opening = null!;
        }

    }
}
