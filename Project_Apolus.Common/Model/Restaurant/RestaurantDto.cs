﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Restaurant
{
    public class RestaurantDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string MainImage { get; set; }

        public bool Status { get; set; }

        public RestaurantDto(Guid id, string name, string address, string mainImage)
        {
            Id = id;
            Name = name;
            Address = address;
            MainImage = mainImage;
        }
    }
}
