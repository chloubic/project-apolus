﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Restaurant
{
    public class RestaurantResultDto
    {

        public IList<RestaurantDto> Data { get; set; } = null!;

        public int Total { get; set; }

    }
}
