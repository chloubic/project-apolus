﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Restaurant
{
    public class UpdateRestaurantDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Address { get; set; }

        public string MainImage { get; set; }

        public UpdateRestaurantDto(Guid id, string name, string description, string address, string mainImage)
        {
            Id = id;
            Name = name;
            Description = description;
            Address = address;
            MainImage = mainImage;
        }
    }
}
