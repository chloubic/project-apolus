﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Tables
{
    public class CreateTableDto
    {

        public string Name { get; set; }

        public CreateTableDto(string name)
        {
            Name = name;
        }

    }
}
