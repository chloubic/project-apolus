﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.Tables
{
    public class TableDto
    {

        public Guid Id { get; set; }

        public string Name { get; set; }

        public TableDto(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

    }
}
