﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.User
{
    public class CreateAdminDto
    {

        public string Name { get; private set; }

        public string Email { get; private set; }

        public string Phone { get; private set; }

        public string Password { get; private set; }

        public string OrganizationName { get; private set; }

        public CreateAdminDto(string name, string email, string phone, string password, string organizationName)
        {
            Name = name;
            Email = email;
            Phone = phone;
            Password = password;
            OrganizationName = organizationName;
        }

    }
}
