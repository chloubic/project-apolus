﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.User
{
    public class CreateClientDto
    {

        public string Name { get; private set; }

        public string Email { get; private set; }

        public string Phone { get; private set; }

        public string Password { get; private set; }

        public CreateClientDto(string name, string email, string phone, string password)
        {
            Name = name;
            Email = email;
            Phone = phone;
            Password = password;
        }

    }
}
