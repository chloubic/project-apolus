﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.User
{
    public class OperatorDto
    {

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public OperatorDto(Guid id,string name, string email, string phoneNumber)
        {
            Id = id;
            Name = name;
            Email = email;
            PhoneNumber = phoneNumber;
        }

    }
}
