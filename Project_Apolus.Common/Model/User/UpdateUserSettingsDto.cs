﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.User
{
    public class UpdateUserSettingsDto
    {

        public Guid Id { get; }
        public string Name { get; }

        public string Email { get; }

        public string PhoneNumber { get; }

        public UpdateUserSettingsDto(Guid id, string name, string email, string phoneNumber)
        {
            Id = id;
            Name = name;
            Email = email;
            PhoneNumber = phoneNumber;
        }
    }
}
