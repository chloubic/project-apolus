﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Model.User
{
    public class UserDto
    {

        public string Name { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public UserDto(string name, string email, string phoneNumber)
        {
            Name = name;
            Email = email;
            PhoneNumber = phoneNumber;
        }

    }
}
