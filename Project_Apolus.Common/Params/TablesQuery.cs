﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.Params
{
    public class TablesQuery
    {
        [BindRequired]
        public DateTime Date { get; set; }

        [BindRequired]
        public DateTime From { get; set; }

        [BindRequired]
        public DateTime To { get; set; }

    }
}
