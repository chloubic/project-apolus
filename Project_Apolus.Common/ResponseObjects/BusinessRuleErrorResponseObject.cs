﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.ResponseObjects
{
    public class BusinessRuleErrorResponseObject : ErrorResponseObject
    {
        public string LocalizedRuleErrorMessage { get; }

        public BusinessRuleErrorResponseObject(string localizedRuleErrorMessage)
            : base("Business rule violated.")
        {
            LocalizedRuleErrorMessage = localizedRuleErrorMessage;
        }
    }
}
