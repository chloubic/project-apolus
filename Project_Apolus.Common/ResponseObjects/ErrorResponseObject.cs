﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.ResponseObjects
{
    public class ErrorResponseObject
    {
        public const string NoErrorInfoMessage = "(No error information received.)";

        public static ErrorResponseObject Empty { get; } = new ErrorResponseObject(NoErrorInfoMessage);

        public string Message { get; }

        public ErrorResponseObject(string message)
        {
            Message = message;
        }
    }
}
