﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.ResponseObjects
{
    public class FieldValidationMessage
    {
        public string FieldName { get; }
        public string Message { get; }

        public FieldValidationMessage(string fieldName, string message)
        {
            FieldName = fieldName;
            Message = message;
        }
    }
}
