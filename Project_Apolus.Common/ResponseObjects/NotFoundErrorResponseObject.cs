﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.ResponseObjects
{
    public class NotFoundErrorResponseObject : ErrorResponseObject
    {
        public string ItemTypeName { get; }

        public Guid ItemId { get; }

        public NotFoundErrorResponseObject(string itemTypeName, Guid itemId) : base("Resource was not found.")
        {
            ItemTypeName = itemTypeName;
            ItemId = itemId;
        }
    }
}
