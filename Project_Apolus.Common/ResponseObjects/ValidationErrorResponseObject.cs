﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.ResponseObjects
{
    public class ValidationErrorResponseObject : ErrorResponseObject
    {
        public ValidationResult? ValidationResult { get; }

        public ValidationErrorResponseObject(ValidationResult validationResult)
            : base("Validation failed.")
        {
            ValidationResult = validationResult;
        }
    }
}
