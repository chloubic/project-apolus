﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.Common.ResponseObjects
{
    public class ValidationResult
    {
        public string Message { get; }

        public ICollection<FieldValidationMessage> FieldValidationMessages { get; }

        public ICollection<string> OverallValidationMessages { get; }

        [JsonConstructor]
        public ValidationResult(string message,
                                ICollection<FieldValidationMessage> fieldValidationMessages,
                                ICollection<string> overallValidationMessages)
        {
            Message = message;
            FieldValidationMessages = fieldValidationMessages;
            OverallValidationMessages = overallValidationMessages;
        }

        public ValidationResult(string message)
        {
            Message = message;
            FieldValidationMessages = new List<FieldValidationMessage>();
            OverallValidationMessages = new List<string>();
        }
    }
}
