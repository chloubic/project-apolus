﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Project_Apolus.DAL.Entities;
using Project_Apolus.DAL.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.DAL
{
    public class AppDbContext : IdentityDbContext<AppUser, AppRole, Guid, AppUserClaim, AppUserRole, AppUserLogin,
        AppRoleClaim, AppUserToken>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public DbSet<OrganizationEntity> Organizations { get; set; } = null!;
        public DbSet<DayEntity> Days { get; set; } = null!;
        public DbSet<OpeningEntity> Openings { get; set; } = null!;
        public DbSet<PhotogalleryEntity> Photogalleries { get; set; } = null!;
        public DbSet<RestaurantEntity> Restaurants { get; set; } = null!;
        public DbSet<MenuEntity> Menus { get; set; } = null!;
        public DbSet<MenuItemEntity> MenuItems { get; set; } = null!;
        public DbSet<ReservationEntity> Reservations { get; set; } = null!;
        public DbSet<TableEntity> Tables { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<AppUserRole>(userRole =>
            {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });
        }
    }
}
