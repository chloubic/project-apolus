﻿using Project_Apolus.BL;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.DAL.Entities
{
    public class DayEntity : IEntity<Guid>
    {
        public Guid Id { get; set; }

        public string Name { get; set; } = null!;

        public int Order { get; set; }

    }
}
