﻿using Microsoft.AspNetCore.Identity;
using Project_Apolus.BL;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.DAL.Entities.Identity
{
    public class AppRole : IdentityRole<Guid>, IEntity<Guid>
    {
        public ICollection<AppUserRole> UserRoles { get; set; } = new List<AppUserRole>();
    }
}
