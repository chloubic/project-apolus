﻿using Microsoft.AspNetCore.Identity;
using Project_Apolus.BL;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.DAL.Entities.Identity
{
    public class AppRoleClaim : IdentityRoleClaim<Guid>, IEntity<int>
    {
    }
}
