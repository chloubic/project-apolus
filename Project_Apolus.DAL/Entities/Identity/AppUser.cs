﻿using Microsoft.AspNetCore.Identity;
using Project_Apolus.BL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Project_Apolus.DAL.Entities.Identity
{
    public class AppUser : IdentityUser<Guid>, IEntity<Guid>, ITimeTrackable
    {
        public string Name { get; set; } = null!;
        
        [ForeignKey(nameof(OrganizationId))]
        public virtual OrganizationEntity Organization { get; set; } = null!;
        
        public Guid OrganizationId { get; set; }
       
        public DateTime CreatedDate { get; set; }
        
        public string RecoveryToken { get; set; } = null!;
        
        public DateTime RecoveryTokenValidity { get; set; }
        public virtual ICollection<AppUserRole> UserRoles { get; set; } = new List<AppUserRole>();
    }
}
