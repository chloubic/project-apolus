﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.DAL.Entities.Identity
{
    public interface ITimeTrackable
    {
        DateTime CreatedDate { get; set; }
    }
}
