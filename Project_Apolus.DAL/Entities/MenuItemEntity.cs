﻿using Project_Apolus.BL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Project_Apolus.DAL.Entities
{
    public class MenuItemEntity : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid MenuId { get; set; }

        [ForeignKey(nameof(MenuId))]
        public MenuEntity Menu { get; set; } = null!;
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;
        public string Weight { get; set; } = null!;
        public string Price { get; set; } = null!;
        public int Order { get; set; }
    }
}
