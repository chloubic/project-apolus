﻿using Project_Apolus.BL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Project_Apolus.DAL.Entities
{
    public class OpeningEntity : IEntity<Guid>
    {
        public Guid Id { get; set; }

        public Guid DayId { get; set; }

        [ForeignKey(nameof(DayId))]
        public virtual DayEntity Day { get; set; } = null!;

        public Guid RestaurantId { get; set; }

        public virtual RestaurantEntity Restaurant { get; set; } = null!;

        public DateTime From { get; set; }

        public DateTime To { get; set; }
    }
}
