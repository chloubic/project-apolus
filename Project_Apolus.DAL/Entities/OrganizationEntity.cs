﻿using Project_Apolus.BL;
using Project_Apolus.DAL.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.DAL.Entities
{
    public class OrganizationEntity : IEntity<Guid>, ITimeTrackable
    {

        public Guid Id { get; set; }

        public string Name { get; set; } = null!;

        public virtual ICollection<AppUser> Users { get; set; } = new List<AppUser>();

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public bool IsClientsOrg { get; set; }

    }
}
