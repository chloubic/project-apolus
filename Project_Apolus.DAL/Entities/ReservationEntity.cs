﻿using Project_Apolus.BL;
using Project_Apolus.DAL.Entities.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Project_Apolus.DAL.Entities
{
    public class ReservationEntity : IEntity<Guid>
    {

        public Guid Id { get; set; }
        public Guid UserId { get; set; } 
        public Guid TableId { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual AppUser User { get; set; } = null!;

        [ForeignKey(nameof(TableId))]
        public virtual TableEntity Table { get; set; } = null!;

        public DateTime Date { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }

    }
}
