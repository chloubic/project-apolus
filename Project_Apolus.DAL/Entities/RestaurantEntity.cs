﻿using Project_Apolus.BL;
using Project_Apolus.DAL.Entities.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Project_Apolus.DAL.Entities
{
    public class RestaurantEntity : IEntity<Guid>, ITimeTrackable
    {

        public Guid Id { get; set; }

        public string Name { get; set; } = null!;

        public string Description { get; set; } = null!;

        public string Address { get; set; } = null!;

        public string MainImage { get; set; } = null!;

        public Guid OrganizationId { get; set; }

        [ForeignKey(nameof(OrganizationId))]
        public virtual OrganizationEntity Organization { get; set; } = null!;

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

    }
}
