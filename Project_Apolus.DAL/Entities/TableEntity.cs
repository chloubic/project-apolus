﻿using Project_Apolus.BL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Project_Apolus.DAL.Entities
{
    public class TableEntity : IEntity<Guid>
    {

        public Guid Id { get; set; }
        public Guid RestaurantId { get; set; }

        [ForeignKey(nameof(RestaurantId))]
        public virtual RestaurantEntity Restaurant { get; set; } = null!;
        public string Name { get; set; } = null!;

        public virtual IList<ReservationEntity> Reservations { get; set; } = null!;
    }
}
