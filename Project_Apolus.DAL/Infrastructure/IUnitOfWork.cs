﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.DAL.Infrastructure
{
    public interface IUnitOfWork
    {
        AppDbContext DbContext { get; }
        Task CommitAsync();
        void Dispose();
    }
}
