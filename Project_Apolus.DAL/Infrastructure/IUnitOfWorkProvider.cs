﻿using Project_Apolus.Common.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.DAL.Infrastructure
{
    public interface IUnitOfWorkProvider
    {
        UnitOfWorkId Create();
        IUnitOfWork Get(UnitOfWorkId uow);
    }
}
