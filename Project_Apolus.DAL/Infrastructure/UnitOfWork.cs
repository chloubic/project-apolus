﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.DAL.Infrastructure
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        public AppDbContext DbContext { get; }

        public UnitOfWork(AppDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public Task CommitAsync()
        {
            return DbContext.SaveChangesAsync();
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                DbContext.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
