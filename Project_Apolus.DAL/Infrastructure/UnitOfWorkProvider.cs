﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Project_Apolus.Common.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Project_Apolus.DAL.Infrastructure
{
    public class UnitOfWorkProvider : IUnitOfWorkProvider
    {
        private readonly IServiceProvider serviceProvider;
        private readonly ILoggerFactory loggerFactory;
        private readonly Func<SqlConnection> connection;

        public ConcurrentDictionary<Guid, IUnitOfWork> Store { get; set; } = new ConcurrentDictionary<Guid, IUnitOfWork>();

        public UnitOfWorkProvider(IServiceProvider serviceProvider, ILoggerFactory loggerFactory, string connectionString)
        {
            this.serviceProvider = serviceProvider;
            this.loggerFactory = loggerFactory;
            connection = () => new SqlConnection(connectionString);
        }

        public UnitOfWorkId Create()
        {
            var guid = Guid.NewGuid();
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlServer(connection(),
                    sqlServerOptionsAction =>
                    {
                        sqlServerOptionsAction.MigrationsAssembly(typeof(AppDbContext).GetTypeInfo().Assembly.GetName().Name);
                    })
                .Options;

            var dbContext = ActivatorUtilities.CreateInstance<AppDbContext>(serviceProvider, options);

            var unitOfWork = new UnitOfWork(dbContext);
            Store.TryAdd(guid, unitOfWork);

            var unitOfWorkId = new UnitOfWorkId(guid);
            unitOfWorkId.Disposing += UnitOfWorkId_Disposing;

            return unitOfWorkId;
        }

        private void UnitOfWorkId_Disposing(Guid id)
        {
            if (Store.TryRemove(id, out var uow))
            {
                uow.Dispose();
            }
        }

        public IUnitOfWork Get(UnitOfWorkId uow)
        {
            if (Store.TryGetValue(uow.Id, out var appDbContext))
            {
                return appDbContext;
            }

            throw new Exception("Unit of work failed to get from the store.");
        }
    }
}
