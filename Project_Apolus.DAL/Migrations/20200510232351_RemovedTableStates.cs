﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Project_Apolus.DAL.Migrations
{
    public partial class RemovedTableStates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tables_TableStates_StateId",
                table: "Tables");

            migrationBuilder.DropTable(
                name: "TableStates");

            migrationBuilder.DropIndex(
                name: "IX_Tables_StateId",
                table: "Tables");

            migrationBuilder.DropColumn(
                name: "StateId",
                table: "Tables");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "StateId",
                table: "Tables",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "TableStates",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TableStates", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Tables_StateId",
                table: "Tables",
                column: "StateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tables_TableStates_StateId",
                table: "Tables",
                column: "StateId",
                principalTable: "TableStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
