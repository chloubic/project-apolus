﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Menu;
using Project_Apolus.DAL.Entities;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.DAL.Repositories
{
    public class MenuItemRepository : RepositoryBase<MenuItem, MenuItemEntity, Guid>, IMenuItemRepository
    {
        public MenuItemRepository(IUnitOfWorkProvider unitOfWorkProvider, IDateTimeProvider dateTimeProvider, IMapper mapper) : base(unitOfWorkProvider, dateTimeProvider, mapper)
        {
        }

        public async Task<bool> HasRightsToUpdateMenuItems(UnitOfWorkId uow, Guid loggedUser, Guid item)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            return await unitOfWork.DbContext.MenuItems.Include(o => o.Menu).ThenInclude(o => o.Restaurant).ThenInclude(o => o.Organization).ThenInclude(o => o.Users)
                .Where(o => o.Id.Equals(item) && o.Menu.Restaurant.Organization.Users.Any(a => a.Id.Equals(loggedUser))).CountAsync() > 0;
        }

        public async Task<bool> HasRightsToCreateMenuItems(UnitOfWorkId uow, Guid loggedUser, Guid menuId)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            return await unitOfWork.DbContext.Menus.Include(o => o.Restaurant).ThenInclude(o => o.Organization).ThenInclude(o => o.Users)
                .Where(o => o.Id.Equals(menuId) && o.Restaurant.Organization.Users.Any(a => a.Id.Equals(loggedUser))).CountAsync() > 0;
        }

        public async Task UpdateMenuItemAsync(UpdateMenuItemDto items, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var entity = await unitOfWork.DbContext.MenuItems.Where(o => o.Id.Equals(items.Id)).FirstOrDefaultAsync();
            if(entity != null)
            {
                Mapper.Map(items, entity);
                unitOfWork.DbContext.Update(entity);
                await unitOfWork.DbContext.SaveChangesAsync();
            }
        }

        public async Task DeleteMenuItemAsync(UnitOfWorkId uow, CancellationToken ct, Guid itemId)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var entity = await unitOfWork.DbContext.MenuItems.Where(o => o.Id.Equals(itemId)).FirstOrDefaultAsync();
            unitOfWork.DbContext.Remove(entity);
            await unitOfWork.DbContext.SaveChangesAsync();
        }

        public async Task ChangeMenuItemsOrderAsync(UnitOfWorkId uow, CancellationToken ct, Guid menuId, IList<ItemOrderUpdateDto> items)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var list = await unitOfWork.DbContext.MenuItems.Where(o => o.MenuId.Equals(menuId)).OrderBy(o => o.Order).ToListAsync();
            var changed = new List<MenuItemEntity>();
            int count = 0;
            foreach (var item in items)
            {
                var order = list.Where(o => o.Id.Equals(item.Id)).FirstOrDefault();
                if (order != null)
                {
                    order.Order = count;
                    changed.Add(order);
                    list.Remove(order);
                    count++;
                }
            }
            foreach (var item in list)
            {
                item.Order = count;
                changed.Add(item);
                count++;
            }
            unitOfWork.DbContext.UpdateRange(changed);
            await unitOfWork.DbContext.SaveChangesAsync();
        }
    }
}
