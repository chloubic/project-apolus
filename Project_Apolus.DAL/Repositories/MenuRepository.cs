﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Menu;
using Project_Apolus.DAL.Entities;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.DAL.Repositories
{
    public class MenuRepository : RepositoryBase<Menu, MenuEntity, Guid>, IMenuRepository
    {
        public MenuRepository(IUnitOfWorkProvider unitOfWorkProvider, IDateTimeProvider dateTimeProvider, IMapper mapper) : base(unitOfWorkProvider, dateTimeProvider, mapper)
        {
        }

        public async Task ChangeMenuCategoryOrderAsync(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId, IList<ItemOrderUpdateDto> items)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var list = await unitOfWork.DbContext.Menus.Where(o => o.RestaurantId.Equals(restaurantId)).OrderBy(o => o.Order).ToListAsync();
            var changed = new List<MenuEntity>();
            int count = 0;
            foreach(var item in items)
            {
                var order = list.Where(o => o.Id.Equals(item.Id)).FirstOrDefault();
                if(order != null)
                {
                    order.Order = count;
                    changed.Add(order);
                    list.Remove(order);
                    count++;
                }
            }
            foreach(var item in list)
            {
                item.Order = count;
                changed.Add(item);
                count++;
            }
            unitOfWork.DbContext.UpdateRange(changed);
            await unitOfWork.DbContext.SaveChangesAsync();
        }

        public async Task DeleteMenuAsync(UnitOfWorkId uow, CancellationToken ct, Guid categoryId)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var entity = await unitOfWork.DbContext.Menus.Where(o => o.Id.Equals(categoryId)).FirstOrDefaultAsync();
            if(entity != null)
            {
                var list = await unitOfWork.DbContext.MenuItems.Where(o => o.MenuId.Equals(categoryId)).ToListAsync();
                unitOfWork.DbContext.RemoveRange(list);
                await unitOfWork.DbContext.SaveChangesAsync();
                unitOfWork.DbContext.Remove(entity);
                await unitOfWork.DbContext.SaveChangesAsync();
            }
        }

        public async Task<ICollection<MenuItem>> GetCategoryItemsByCategoryId(UnitOfWorkId uow, CancellationToken ct, Guid categoryId)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var list = await unitOfWork.DbContext.MenuItems.Where(o => o.MenuId.Equals(categoryId)).OrderBy(o => o.Order).ToListAsync();

            return Mapper.Map<ICollection<MenuItem>>(list);
        }

        public async Task<ICollection<Menu>> GetMenuCategoriesByRestaurantId(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var list = await unitOfWork.DbContext.Menus.Where(o => o.RestaurantId.Equals(restaurantId)).OrderBy(o => o.Order).ToListAsync();

            return Mapper.Map<ICollection<Menu>>(list);
        }

        public async Task<bool> HasRightsToCreateMenu(UnitOfWorkId uow, Guid loggedUser, Guid restaurantId)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            return await unitOfWork.DbContext.Restaurants.Include(o => o.Organization).ThenInclude(o => o.Users)
                .Where(o => o.Id.Equals(restaurantId) && o.Organization.Users.Any(a => a.Id.Equals(loggedUser))).CountAsync() > 0;
        }

        public async Task<bool> HasRightsToUpdateMenu(UnitOfWorkId uow, Guid user, Guid item)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            return await unitOfWork.DbContext.Menus.Include(o => o.Restaurant).ThenInclude(o => o.Organization).ThenInclude(o => o.Users)
                .Where(o => o.Id.Equals(item) && o.Restaurant.Organization.Users.Any(a => a.Id.Equals(user))).CountAsync() > 0;
        }

        public async Task UpdateMenuAsync(UpdateMenuDto categories, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var entity = await unitOfWork.DbContext.Menus.Where(o => o.Id.Equals(categories.Id)).FirstOrDefaultAsync();
            if (entity != null)
            {
                Mapper.Map(categories, entity);
                unitOfWork.DbContext.Update(entity);
                await unitOfWork.DbContext.SaveChangesAsync();
            }
        }
    }
}
