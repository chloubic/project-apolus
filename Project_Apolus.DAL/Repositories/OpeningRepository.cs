﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Opening;
using Project_Apolus.DAL.Entities;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.DAL.Repositories
{
    public class OpeningRepository : RepositoryBase<Opening, OpeningEntity, Guid>, IOpeningRepository
    {
        public OpeningRepository(IUnitOfWorkProvider unitOfWorkProvider, IDateTimeProvider dateTimeProvider, IMapper mapper) : base(unitOfWorkProvider, dateTimeProvider, mapper)
        {
        }

        public async Task EraseRestaurantOpeningHours(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOFWork = UnitOfWorkProvider.Get(uow);

            var openingList = await unitOFWork.DbContext.Openings.Where(o => o.RestaurantId.Equals(id)).ToListAsync();

            unitOFWork.DbContext.RemoveRange(openingList);
        }

        public async Task<IList<Opening>> GetRestaurantOpeningHours(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var entityList = await unitOfWork.DbContext.Openings.Where(o => o.RestaurantId.Equals(id)).ToListAsync();

            return Mapper.Map<IList<Opening>>(entityList);
        }

        public async Task UpdateRestaurantOpeningHours(List<Opening> openingList, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOFWork = UnitOfWorkProvider.Get(uow);

            var entityList = Mapper.Map<IList<OpeningEntity>>(openingList);

            await unitOFWork.DbContext.AddRangeAsync(entityList);

            await unitOFWork.DbContext.SaveChangesAsync();
        }

        public async Task<string> GetDayNameByIdAsync(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOFWork = UnitOfWorkProvider.Get(uow);
            return (await unitOFWork.DbContext.Days.Where(o => o.Id.Equals(id)).FirstOrDefaultAsync()).Name;
        }

        private bool TimeBetween(DateTime datetime, TimeSpan start, TimeSpan end)
        {
            // convert datetime to a TimeSpan
            TimeSpan now = datetime.TimeOfDay;
            // see if start comes before end
            if (start < end)
                return start <= now && now <= end;
            // start is after end, so do the inverse comparison
            return !(end < now && now < start);
        }

        public async Task<bool> GetRestaurantStatusAsync(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOFWork = UnitOfWorkProvider.Get(uow);
            var day = (int)(DateTime.Now.DayOfWeek + 6) % 7;
            return await unitOFWork.DbContext.Openings.Include(o => o.Day)
                .Where(o => o.RestaurantId.Equals(id) && o.Day.Order.Equals(day) && 
                o.From.TimeOfDay.CompareTo(DateTimeProvider.Now.TimeOfDay) <= 0 && o.To.TimeOfDay.CompareTo(DateTimeProvider.Now.TimeOfDay) > 0)
                .CountAsync() > 0;
        }

        public async Task<IList<DayDto>> GetDays(UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOFWork = UnitOfWorkProvider.Get(uow);
            var list = await unitOFWork.DbContext.Days.OrderBy(o => o.Order).ToListAsync();
            return Mapper.Map<IList<DayDto>>(list);
        }
    }
}
