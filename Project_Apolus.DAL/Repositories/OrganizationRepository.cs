﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Organization;
using Project_Apolus.DAL.Entities;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.DAL.Repositories
{
    public class OrganizationRepository : RepositoryBase<Organization, OrganizationEntity, Guid>, IOrganizationRepository
    {
        public OrganizationRepository(IUnitOfWorkProvider unitOfWorkProvider, IDateTimeProvider dateTimeProvider, IMapper mapper) : base(unitOfWorkProvider, dateTimeProvider, mapper)
        {
        }

        public async Task CreateAsync(Organization organization, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var entity = Mapper.Map<OrganizationEntity>(organization);

            await unitOfWork.DbContext.AddAsync(entity, ct);

            await unitOfWork.DbContext.SaveChangesAsync();
        }

        public async Task<Guid> GetClientOrganizationGuid(UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var entity = await unitOfWork.DbContext.Organizations.Where(o => o.IsClientsOrg).FirstOrDefaultAsync();

            return entity.Id;
        }

        public async Task UpdateOrganization(UnitOfWorkId uow, CancellationToken ct, UpdateOrganizationDto update, Guid orgId)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var entity = await unitOfWork.DbContext.Organizations.Where(o => o.Id.Equals(orgId)).FirstOrDefaultAsync();

            Mapper.Map(update, entity);

            entity.UpdatedDate = DateTimeProvider.Now;

            unitOfWork.DbContext.Update(entity);

            await unitOfWork.DbContext.SaveChangesAsync();
        }
    }
}
