﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.DAL.Entities;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.DAL.Repositories
{
    public class PhotogalleryRepository : RepositoryBase<Photo, PhotogalleryEntity, Guid>, IPhotogalleryRepository
    {
        public PhotogalleryRepository(IUnitOfWorkProvider unitOfWorkProvider, IDateTimeProvider dateTimeProvider, IMapper mapper) : base(unitOfWorkProvider, dateTimeProvider, mapper)
        {
        }

        public async Task EraseGalleryAsync(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var photoList = await unitOfWork.DbContext.Photogalleries.Where(o => o.RestaurantId.Equals(id)).ToListAsync();

            unitOfWork.DbContext.RemoveRange(photoList);
        }

        public async Task<IList<Photo>> GetRestaurantPhotos(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var entityList = await unitOfWork.DbContext.Photogalleries.Where(o => o.RestaurantId.Equals(id)).ToListAsync();

            return Mapper.Map<IList<Photo>>(entityList);
        }

        public async Task UpdateGalleryAsync(List<Photo> photoList, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var entityList = Mapper.Map<IList<PhotogalleryEntity>>(photoList);

            await unitOfWork.DbContext.AddRangeAsync(entityList);

            await unitOfWork.DbContext.SaveChangesAsync();
        }
    }
}
