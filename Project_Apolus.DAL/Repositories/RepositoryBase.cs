﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Project_Apolus.BL;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.DAL.Entities.Identity;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.DAL.Repositories
{
    public class RepositoryBase<TDomainObject, TEntity, TKey> : IRepository<TDomainObject, TKey>
        where TEntity : class, IEntity<TKey>, new()
        where TDomainObject : class, IDomainObject<TKey>
    {
        protected IUnitOfWorkProvider UnitOfWorkProvider { get; }
        protected IMapper Mapper { get; }
        protected IDateTimeProvider DateTimeProvider { get; }

        public RepositoryBase(IUnitOfWorkProvider unitOfWorkProvider, IDateTimeProvider dateTimeProvider, IMapper mapper)
        {
            UnitOfWorkProvider = unitOfWorkProvider;
            Mapper = mapper;
            DateTimeProvider = dateTimeProvider;
        }

        public virtual async Task<TDomainObject> GetByIdAsync(TKey id, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var entity = await unitOfWork.DbContext.Set<TEntity>().FirstOrDefaultAsync(r => Equals(r.Id, id), ct);
            return Mapper.Map<TDomainObject>(entity);
        }

        public virtual void Update(TDomainObject domainObject, UnitOfWorkId uow)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            unitOfWork.DbContext.Entry(domainObject).State = EntityState.Modified;
        }

        public virtual async Task InsertAsync(TDomainObject domainObject, UnitOfWorkId uow, CancellationToken ct)
        {
            if (domainObject is ITimeTrackable timeEntity)
            {
                timeEntity.CreatedDate = DateTimeProvider.Now;
            }

            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var entity = Mapper.Map<TEntity>(domainObject);
            await unitOfWork.DbContext.Set<TEntity>().AddAsync(entity, ct);
            await unitOfWork.DbContext.SaveChangesAsync();
        }

        public virtual void Delete(TKey id, UnitOfWorkId uow)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            // try to get entity from the context
            var entity = unitOfWork.DbContext.Set<TEntity>().Local.SingleOrDefault(e => e.Id!.Equals(id));

            // if entity is not found in the context, create fake entity and attach it
            if (entity == null)
            {
                entity = new TEntity { Id = id };
                unitOfWork.DbContext.Set<TEntity>().Attach(entity);
            }

            Delete(entity, uow);
            unitOfWork.DbContext.SaveChanges();
        }

        public virtual void Delete(TDomainObject domainObject, UnitOfWorkId uow)
        {
            var entity = Mapper.Map<TEntity>(domainObject);
            Delete(entity, uow);
        }

        private void Delete(TEntity entity, UnitOfWorkId uow)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            unitOfWork.DbContext.Set<TEntity>().Remove(entity);
            unitOfWork.DbContext.SaveChanges();
        }

        public virtual async Task<List<TDomainObject>> GetAllAsync(UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var entities = await unitOfWork.DbContext.Set<TEntity>().ToListAsync(ct);
            return entities.Select(x => Mapper.Map<TDomainObject>(x)).ToList();
        }

        public virtual async Task<int> GetCountAsync(UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            return await unitOfWork.DbContext.Set<TEntity>().CountAsync(ct);
        }

        public Task<IList<TDomainObject>> GetByIdsAsync(IEnumerable<TKey> ids, UnitOfWorkId uow, CancellationToken ct)
        {
            throw new NotImplementedException();
        }

        public async Task InsertAsync(IEnumerable<TDomainObject> domainObjects, UnitOfWorkId uow, CancellationToken ct)
        {
            foreach (var domainObject in domainObjects.ToList())
            {
                await InsertAsync(domainObject, uow, ct);
            }
        }

        public void Update(IEnumerable<TDomainObject> domainObjects, UnitOfWorkId uow)
        {
            foreach (var domainObject in domainObjects.ToList())
            {
                Update(domainObject, uow);
            }
        }

        public void Delete(IEnumerable<TKey> ids, UnitOfWorkId uow)
        {
            foreach (var id in ids)
            {
                Delete(id, uow);
            }
        }

        public void Delete(IEnumerable<TDomainObject> domainObjects, UnitOfWorkId uow)
        {
            foreach (var domainObject in domainObjects.ToList())
            {
                Delete(domainObject, uow);
            }
        }
    }
}
