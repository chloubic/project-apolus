﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Project_Apolus.BL;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Reservations;
using Project_Apolus.DAL.Entities;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.DAL.Repositories
{
    public class ReservationRepository : RepositoryBase<Reservation, ReservationEntity, Guid>, IReservationRepository
    {
        public ReservationRepository(IUnitOfWorkProvider unitOfWorkProvider, IDateTimeProvider dateTimeProvider, IMapper mapper) : base(unitOfWorkProvider, dateTimeProvider, mapper)
        {
        }

        public async Task<bool> CanDelete(Guid userId, Guid reservationId, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            return await unitOfWork.DbContext.Reservations
                .Where(o => o.Id.Equals(reservationId) && o.UserId.Equals(userId)).CountAsync() > 0;
        }

        public async Task<bool> CanReservate(Guid tableId, DateTime date, DateTime from, DateTime to, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var table = await unitOfWork.DbContext.Tables.Include(o => o.Reservations)
                .Where(o => o.Id.Equals(tableId)).FirstOrDefaultAsync();

            if (table != null)
            {
                return !table.Reservations.Any(o => o.Date.Date.Equals(date.Date)
                    && (TimeExtensions.IsBetween(from, o.From, o.To) || TimeExtensions.IsBetween(to, o.From, o.To)));
            }
            return false;
        }

        public async Task CreateAsync(UnitOfWorkId uow, CancellationToken ct, Reservation reservation)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var entity = Mapper.Map<ReservationEntity>(reservation);

            await unitOfWork.DbContext.AddAsync(entity);

            await unitOfWork.DbContext.SaveChangesAsync();

        }

        public async Task DeleteAsync(UnitOfWorkId uow, CancellationToken ct, Guid reservationId, Guid userId)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var entity = await unitOfWork.DbContext.Reservations.Where(o => o.Id.Equals(reservationId) && o.UserId.Equals(userId)).FirstOrDefaultAsync();

            if (entity != null)
            {
                unitOfWork.DbContext.Remove(entity);
            }

            await unitOfWork.DbContext.SaveChangesAsync();
        }

        public async Task<IList<ReservationDto>> GetMyReservationsAsync(UnitOfWorkId uow, CancellationToken ct, Guid id)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var date = DateTimeProvider.Now.Date;
            var time = DateTimeProvider.Now.TimeOfDay;

            var list = await unitOfWork.DbContext.Reservations.Include(o => o.User).Include(o => o.Table).ThenInclude(o => o.Restaurant)
                .Where(o => o.UserId.Equals(id) && (DateTime.Compare(o.Date.Date, DateTimeProvider.Now.Date) > 0 || (o.Date.Date.Equals(DateTimeProvider.Now.Date)
            && o.To.TimeOfDay >= DateTimeProvider.Now.TimeOfDay))).ToListAsync();

            return Mapper.Map<IList<ReservationDto>>(list);
        }

        public async Task<IList<ReservationDto>> GetRestaurantReservationAsync(UnitOfWorkId uow, CancellationToken ct, Guid restaurantId)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var list = await unitOfWork.DbContext.Reservations.Include(o => o.User).Include(o => o.Table).ThenInclude(o => o.Restaurant)
                .Where(o => o.Table.RestaurantId.Equals(restaurantId) && (DateTime.Compare(o.Date.Date, DateTimeProvider.Now.Date) > 0 || (o.Date.Date.Equals(DateTimeProvider.Now.Date)
            && o.To.TimeOfDay >= DateTimeProvider.Now.TimeOfDay)))
                .OrderBy(o => o.Date.Date).OrderBy(o => o.From.TimeOfDay).OrderBy(o => o.To.TimeOfDay).ToListAsync();

            return Mapper.Map<IList<ReservationDto>>(list);
        }
    }
}
