﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Filters;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Restaurant;
using Project_Apolus.DAL.Entities;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.DAL.Repositories
{
    public class RestaurantRepository : RepositoryBase<Restaurant, RestaurantEntity, Guid>, IRestaurantRepository
    {
        public RestaurantRepository(IUnitOfWorkProvider unitOfWorkProvider, IDateTimeProvider dateTimeProvider, IMapper mapper) : base(unitOfWorkProvider, dateTimeProvider, mapper)
        {
        }

        public async Task<bool> CanManipulate(UnitOfWorkId uow, CancellationToken ct, Guid id, Guid restaurantId)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            return await unitOfWork.DbContext.Restaurants.Include(o => o.Organization).ThenInclude(o => o.Users).Where(o => o.Id.Equals(restaurantId)
            && o.Organization.Users.Any(a => a.Id.Equals(id))).CountAsync() > 0;
        }

        public async Task CreateAsync(Restaurant restaurant, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var entity = Mapper.Map<RestaurantEntity>(restaurant);
            unitOfWork.DbContext.Restaurants.Add(entity);
            await unitOfWork.DbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var restaurant = await unitOfWork.DbContext.Restaurants.Where(o => o.Id.Equals(id)).FirstOrDefaultAsync();
            var photogallery = await unitOfWork.DbContext.Photogalleries.Where(o => o.RestaurantId.Equals(id)).ToListAsync();
            var opening = await unitOfWork.DbContext.Openings.Where(o => o.RestaurantId.Equals(id)).ToListAsync();

            unitOfWork.DbContext.RemoveRange(photogallery);
            unitOfWork.DbContext.RemoveRange(opening);
            unitOfWork.DbContext.Remove(restaurant);

            await unitOfWork.DbContext.SaveChangesAsync();

        }

        public async Task<IList<Restaurant>> GetFilteredRestaurantsAsync(RestaurantFilter filter, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var query = unitOfWork.DbContext.Restaurants.Skip((filter.Page - 1)*filter.Limit).Take(filter.Limit).AsQueryable();

            if (!string.IsNullOrEmpty(filter.Name)) query = query.Where(o => o.Name.Contains(filter.Name));
            if (filter.DescOrder) query = query.OrderByDescending(o => o.Name);
            else query = query.OrderBy(o => o.Name);

            return Mapper.Map<IList<Restaurant>>(await query.ToListAsync());
        }

        public async Task<int> GetFilteredRestaurantsCountAsync(RestaurantFilter filter, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var query = unitOfWork.DbContext.Restaurants.AsQueryable();

            if (!string.IsNullOrEmpty(filter.Name)) query = query.Where(o => o.Name.Contains(filter.Name));
            if (filter.DescOrder) query = query.OrderByDescending(o => o.Name);
            else query = query.OrderBy(o => o.Name);

            return await query.CountAsync();
        }

        public async Task<Restaurant> GetRestaurantDetail(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var restaurant = await unitOfWork.DbContext.Restaurants.Where(o => o.Id.Equals(id)).FirstOrDefaultAsync();

            return Mapper.Map<Restaurant>(restaurant);
        }

        public async Task<IList<Restaurant>> GetRestaurantsByOrganization(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var restaurants = await unitOfWork.DbContext.Restaurants.Where(o => o.OrganizationId.Equals(id)).ToListAsync();

            return Mapper.Map<IList<Restaurant>>(restaurants);
        }

        public async Task UpdateAsync(UnitOfWorkId uow, CancellationToken ct, UpdateRestaurantDto updateRestaurantDto)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var entity = await unitOfWork.DbContext.Restaurants.Where(o => o.Id.Equals(updateRestaurantDto.Id)).FirstOrDefaultAsync();

            if(entity != null)
            {
                Mapper.Map(updateRestaurantDto, entity);
                unitOfWork.DbContext.Update(entity);
                await unitOfWork.DbContext.SaveChangesAsync();
            }
        }
    }
}
