﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Project_Apolus.BL;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Params;
using Project_Apolus.DAL.Entities;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.DAL.Repositories
{
    public class TableRepository : RepositoryBase<Table, TableEntity, Guid>, ITableRepository
    {
        public TableRepository(IUnitOfWorkProvider unitOfWorkProvider, IDateTimeProvider dateTimeProvider, IMapper mapper) : base(unitOfWorkProvider, dateTimeProvider, mapper)
        {
        }

        public async Task<bool> CanDelete(Guid userId, Guid tableId, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            return await unitOfWork.DbContext.Tables.Include(o => o.Restaurant).ThenInclude(o => o.Organization).ThenInclude(o => o.Users)
                .Where(o => o.Id.Equals(tableId) && o.Restaurant.Organization.Users.Any(a => a.Id.Equals(userId))).CountAsync() > 0;
        }

        public async Task<bool> CanManipulate(Guid userId, Guid restaurantId, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            return await unitOfWork.DbContext.Restaurants.Include(o => o.Organization).ThenInclude(o => o.Users)
                .Where(o => o.Id.Equals(restaurantId) && o.Organization.Users.Any(a => a.Id.Equals(userId))).CountAsync() > 0;
        }

        public async Task CreateAsync(Table tableModel, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var entity = Mapper.Map<TableEntity>(tableModel);
            unitOfWork.DbContext.Add(entity);
            await unitOfWork.DbContext.SaveChangesAsync();

        }

        public async Task DeleteByIdAsync(Guid tableId, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var entity = await unitOfWork.DbContext.Tables.Where(o => o.Id.Equals(tableId)).FirstOrDefaultAsync();

            if(entity != null)
            {
                unitOfWork.DbContext.Remove(entity);
                await unitOfWork.DbContext.SaveChangesAsync();
            }
        }

        public async Task<IList<Table>> GetByRestaurantAsync(Guid restaurantId, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            return Mapper.Map<IList<Table>>(await unitOfWork.DbContext.Tables.Where(o => o.RestaurantId.Equals(restaurantId)).ToListAsync());
        }

        public async Task<IList<Table>> GetAvailableTablesAsync(UnitOfWorkId uow, CancellationToken ct, Guid id, TablesQuery param)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var list = await unitOfWork.DbContext.Tables.Include(o => o.Reservations)
                .Where(o => o.RestaurantId.Equals(id)).ToListAsync();

            var result = new List<TableEntity>();

            foreach (var table in list)
            {
                var canUse = !table.Reservations.Any(o => o.Date.Date.Equals(param.Date.Date)
                && (TimeExtensions.IsBetween(param.From, o.From, o.To) || TimeExtensions.IsBetween(param.To, o.From, o.To)));

                if (canUse) result.Add(table);
            }    

            return Mapper.Map<IList<Table>>(result);
        }
    }
}
