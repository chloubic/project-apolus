﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Project_Apolus.BL.DateTimeProviders;
using Project_Apolus.BL.Model;
using Project_Apolus.BL.Repositories;
using Project_Apolus.Common;
using Project_Apolus.Common.Enums;
using Project_Apolus.Common.Exceptions;
using Project_Apolus.Common.Infrastructure;
using Project_Apolus.Common.Model.Auth;
using Project_Apolus.Common.Model.User;
using Project_Apolus.Common.ResponseObjects;
using Project_Apolus.DAL.Entities;
using Project_Apolus.DAL.Entities.Identity;
using Project_Apolus.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Apolus.DAL.Repositories
{
    public class UserRepository : RepositoryBase<User, AppUser, Guid>, IUserRepository
    {
        private readonly UserManager<AppUser> userManager;
        private readonly IUserStore<AppUser> userStore;

        public UserRepository(IUnitOfWorkProvider unitOfWorkProvider,
            IDateTimeProvider dateTimeProvider,
            UserManager<AppUser> userManager,
            IMapper mapper, IUserStore<AppUser> userStore)
            : base(unitOfWorkProvider,
                dateTimeProvider,
                mapper)
        {
            this.userStore = userStore;
            this.userManager = userManager;
        }

        public override void Update(User domainObject, UnitOfWorkId uow)
        {
            throw new NotSupportedException();
        }

        public override Task InsertAsync(User domainObject, UnitOfWorkId uow, CancellationToken ct)
        {
            throw new NotSupportedException();
        }

        public async Task<bool> CheckRoleAsync(User user, string role)
        {
            return await userManager.IsInRoleAsync(Mapper.Map<AppUser>(user), role);
        }

        public async Task CreateWithPasswordAsync(User user, string password, string[] roles, UnitOfWorkId uow)
        {
            var entity = Mapper.Map<AppUser>(user);
            entity.RecoveryToken = "";
            entity.RecoveryTokenValidity = DateTime.Now;

            entity.UserName = RemoveDiacritics(user.Email);

            await CreateUserAsync(entity, password);

            await AddUserToRoleAsync(entity, roles);

            await AddClaimsToUserAsync(entity, roles, uow);
        }

        public async Task CreateWithPasswordAndOrganizationAsync(User user, string password, string[] roles, UnitOfWorkId uow, Organization organization)
        {
            var entity = Mapper.Map<AppUser>(user);
            entity.RecoveryToken = "";
            entity.RecoveryTokenValidity = DateTime.Now;

            entity.UserName = RemoveDiacritics(user.Email);

            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var organizationEntity = Mapper.Map<OrganizationEntity>(organization);
            unitOfWork.DbContext.Organizations.Add(organizationEntity);
            await unitOfWork.DbContext.SaveChangesAsync();

            try
            {
                await CreateUserAsync(entity, password);

                await AddUserToRoleAsync(entity, roles);

                await AddClaimsToUserAsync(entity, roles, uow);
            }
            catch (ValidationFailedException ex)
            {
                unitOfWork.DbContext.Organizations.Remove(organizationEntity);
                await unitOfWork.DbContext.SaveChangesAsync();
                throw;
            }
        }

        public async Task ChangeEmailAsync(Guid userId, string newEmail)
        {
            var user = await userManager.FindByIdAsync(userId.ToString());
            if (!user.Email.Equals(newEmail))
            {
                var result = await userManager.SetEmailAsync(user, newEmail);
                EnsureIdentityOperationSucceeded(result);
                result = await userManager.SetUserNameAsync(user, newEmail);
                EnsureIdentityOperationSucceeded(result);
            }
        }

        public async Task ChangePhoneNumberAsync(Guid userId, string newPhone)
        {
            var user = await userManager.FindByIdAsync(userId.ToString());
            if (!user.PhoneNumber.Equals(newPhone))
            {
                var result = await userManager.SetPhoneNumberAsync(user, newPhone);
                EnsureIdentityOperationSucceeded(result);
            }
        }
        public async Task ChangeNameAsync(Guid userId, string newName)
        {
            var user = await userManager.FindByIdAsync(userId.ToString());
            if (!user.Name.Equals(newName))
            {
                user.Name = newName;
                var result = await userManager.UpdateAsync(user);
                EnsureIdentityOperationSucceeded(result);
            }
        }

        public async Task ChangePasswordAsync(ChangePasswordDto dto, Guid userId)
        {
            var user = await userManager.FindByIdAsync(userId.ToString());

            if (user == null)
            {
                throw new ItemNotFoundException(nameof(AppUser), userId);
            }

            //if (user.PasswordState == PasswordState.RegularPassword)
            //{
            //    var result = await userManager.ChangePasswordAsync(user, dto.OldPassword, dto.NewPassword);
            //    EnsureIdentityOperationSucceeded(result);
            //}
            //else
            //{
            //    await SetPasswordAsync(user, dto.NewPassword);
            //    user.PasswordState = PasswordState.RegularPassword;
            //    await UpdateAsync(user);
            //}

            var result = await userManager.ChangePasswordAsync(user, dto.OldPassword, dto.NewPassword);
            EnsureIdentityOperationSucceeded(result);
        }

        private static string RemoveDiacritics(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            text = text.Normalize(NormalizationForm.FormD);
            var chars = text.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark).ToArray();
            return new string(chars).Normalize(NormalizationForm.FormC);
        }

        private async Task AddClaimsToUserAsync(AppUser user, string[] roles, UnitOfWorkId uow)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var organization = await unitOfWork.DbContext.Organizations.FindAsync(user.OrganizationId);
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, RemoveDiacritics(user.Name) ?? string.Empty),
                new Claim(CustomClaimTypes.OrganizationId, organization.Id.ToString())
            };
            claims.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role)));

            var result = await userManager.AddClaimsAsync(user, claims);
            EnsureIdentityOperationSucceeded(result);
        }

        private async Task AddUserToRoleAsync(AppUser user, string[] roles)
        {
            var result = await userManager.AddToRolesAsync(user, roles);
            EnsureIdentityOperationSucceeded(result);
        }

        private async Task CreateUserAsync(AppUser user, string password) //, PasswordState passwordState)
        {
            user.CreatedDate = DateTimeProvider.Now;
            var result = await userManager.CreateAsync(user, password);
            EnsureIdentityOperationSucceeded(result);
        }

        private static void EnsureIdentityOperationSucceeded(IdentityResult result)
        {
            if (!result.Succeeded)
            {
                throw new ValidationFailedException(new ValidationResult("Validation failed.",
                    new List<FieldValidationMessage>(),
                    result.Errors.Select(x => x.Description).ToList()));
            }
        }

        //public async Task SetOneTimePasswordAsync(int userId, string password)
        //{
        //    var user = await userManager.FindByIdAsync(userId.ToString());
        //    await SetPasswordAsync(user, password);
        //    user.PasswordState = PasswordState.OneTimePasswordActive;
        //    await UpdateAsync(user);
        //}

        public async Task SetPasswordAsync(User user, string password)
        {
            var appUser = await userManager.FindByIdAsync(user.Id.ToString());
            var result = await userManager.RemovePasswordAsync(appUser);
            EnsureIdentityOperationSucceeded(result);
            result = await userManager.AddPasswordAsync(appUser, password);
            EnsureIdentityOperationSucceeded(result);
        }

        public async Task SetPasswordByTokenAsync(string token, string password, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var appUser = await unitOfWork.DbContext.Set<AppUser>().Where(o => o.RecoveryToken == token).Where(o => o.RecoveryTokenValidity.CompareTo(DateTime.Now) >= 0).FirstOrDefaultAsync();

            if (appUser == null) return;

            var entity = await userManager.FindByIdAsync(appUser.Id.ToString());

            var result = await userManager.RemovePasswordAsync(entity);
            EnsureIdentityOperationSucceeded(result);
            result = await userManager.AddPasswordAsync(entity, password);
            EnsureIdentityOperationSucceeded(result);
        }

        public async Task ChangeRoleAsync(Guid id, string role, UnitOfWorkId uow)
        {
            var appUser = await userManager.FindByIdAsync(id.ToString());
            var oldRoles = await userManager.GetRolesAsync(appUser);
            var result = await userManager.RemoveFromRolesAsync(appUser, oldRoles);
            EnsureIdentityOperationSucceeded(result);
            result = await userManager.AddToRoleAsync(appUser, role);
            EnsureIdentityOperationSucceeded(result);
        }

        public async Task<bool> UserExistsAsync(Guid id, UnitOfWorkId uow, CancellationToken ct)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var count = await unitOfWork.DbContext.Users.Where(o => o.Id == id).CountAsync();
            if (count > 0)
            {
                return true;
            }
            return false;
        }

        public async Task DeleteAsync(Guid id, UnitOfWorkId uow)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var user = await unitOfWork.DbContext.Users.Where(o => o.Id == id).FirstOrDefaultAsync();

            unitOfWork.DbContext.Remove(user);

            await unitOfWork.DbContext.SaveChangesAsync();
        }

        public async Task<User> GetByEmailAsync(UnitOfWorkId uow, CancellationToken ct, string email)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var userEntity = await unitOfWork.DbContext.Users.Where(o => o.Email == email).FirstOrDefaultAsync();

            return Mapper.Map<User>(userEntity);
        }

        public async Task<User> GetUserByRecoveryToken(UnitOfWorkId uow, CancellationToken ct, string token)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var userEntity = await unitOfWork.DbContext.Users
                .Where(o => o.RecoveryToken == token && o.RecoveryTokenValidity.CompareTo(DateTimeProvider.Now) > 0)
                .FirstOrDefaultAsync();

            return Mapper.Map<User>(userEntity);
        }

        public async Task<string> GetUserRoleAsync(UnitOfWorkId uow, CancellationToken ct, Guid id)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var role = await unitOfWork.DbContext.UserRoles.Where(o => o.UserId == id).Include(o => o.Role).FirstOrDefaultAsync();
            return role.Role.Name;
        }

        public async Task UpdateUserAsync(UpdateUserSettingsDto user, UnitOfWorkId uow)
        {
            var entity = await userManager.FindByIdAsync(user.Id.ToString());

            if (!string.IsNullOrEmpty(user.Email) && !entity.Email.Equals(user.Email))
            {
                entity.Email = user.Email;
                entity.NormalizedEmail = user.Email.ToUpper();
            }
            if (!string.IsNullOrEmpty(user.PhoneNumber) && !entity.PhoneNumber.Equals(user.PhoneNumber)) entity.PhoneNumber = user.PhoneNumber;
            if (!string.IsNullOrEmpty(user.Name) && !entity.Name.Equals(user.Name)) entity.Name = user.Name;

            var result = await userManager.UpdateAsync(entity);
            EnsureIdentityOperationSucceeded(result);
        }

        public async Task<bool> UpdateUserTokenAsync(UnitOfWorkId uow, CancellationToken ct, string email, Guid token)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var entity = await unitOfWork.DbContext.Users.Where(o => o.Email == email).FirstOrDefaultAsync();

            if (entity == null) return false;

            entity.RecoveryToken = token.ToString();
            entity.RecoveryTokenValidity = DateTime.Now.AddHours(1);

            unitOfWork.DbContext.Update(entity);

            return true;
        }

        public async Task DeleteClientAsync(Guid id, UnitOfWorkId uow)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);

            var reservations = await unitOfWork.DbContext.Reservations.Where(o => o.UserId.Equals(id)).ToListAsync();
            unitOfWork.DbContext.RemoveRange(reservations);

            var user = await unitOfWork.DbContext.Users.Where(o => o.Id.Equals(id)).FirstOrDefaultAsync();
            unitOfWork.DbContext.Remove(user);

            await unitOfWork.DbContext.SaveChangesAsync();
        }

        public async Task DeleteAdminAsync(Guid id, UnitOfWorkId uow)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var user = await unitOfWork.DbContext.Users.Where(o => o.Id.Equals(id)).FirstOrDefaultAsync();

            var orgId = user.OrganizationId;

            var reservations = await unitOfWork.DbContext.Reservations.Include(o => o.Table).ThenInclude(o => o.Restaurant)
                .Where(o => o.Table.Restaurant.OrganizationId.Equals(orgId)).ToListAsync();
            unitOfWork.DbContext.RemoveRange(reservations);

            var restaurants = await unitOfWork.DbContext.Restaurants.Where(o => o.OrganizationId.Equals(orgId)).ToListAsync();
            unitOfWork.DbContext.RemoveRange(restaurants);

            var users = await unitOfWork.DbContext.Users.Where(o => o.OrganizationId.Equals(orgId)).ToListAsync();
            unitOfWork.DbContext.RemoveRange(users);

            unitOfWork.DbContext.Remove(user);

            var organization = await unitOfWork.DbContext.Organizations.Where(o => o.Id.Equals(orgId)).FirstOrDefaultAsync();
            unitOfWork.DbContext.Remove(organization);

            await unitOfWork.DbContext.SaveChangesAsync();
        }

        public async Task UpdateUserInfoAsync(UnitOfWorkId uow, CancellationToken ct, UserDto user, Guid id)
        {
            await ChangeEmailAsync(id, user.Email);
            await ChangePhoneNumberAsync(id, user.PhoneNumber);
            await ChangeNameAsync(id, user.Name);
        }

        public async Task<IList<User>> GetOperatorsAsync(UnitOfWorkId uow, CancellationToken ct, Guid organizationId)
        {
            var unitOfWork = UnitOfWorkProvider.Get(uow);
            var list = await unitOfWork.DbContext.Users.Include(o => o.UserRoles).ThenInclude(o => o.Role)
                .Where(o => o.OrganizationId.Equals(organizationId) && o.UserRoles.Any(a => a.Role.Name.Equals(Roles.Operator))).ToListAsync();
            return Mapper.Map<IList<User>>(list);
        }
    }
}
