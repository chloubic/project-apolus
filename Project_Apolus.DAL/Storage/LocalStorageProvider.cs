﻿using Microsoft.AspNetCore.Hosting;
using Project_Apolus.BL;
using Project_Apolus.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Project_Apolus.DAL.Storage
{
    public class LocalStorageProvider : IStorage
    {
        private IWebHostEnvironment _hostingEnvironment;
        public LocalStorageProvider(IWebHostEnvironment environment)
        {
            _hostingEnvironment = environment;
        }

        public async Task<Byte[]> GetAsync(string filename)
        {
            var uploads = Path.Combine(_hostingEnvironment.ContentRootPath, "Uploads");
            var filePath = Path.Combine(uploads, filename);
            if (!File.Exists(filePath)) throw new ItemNotFoundException("Image", Guid.Empty);
            Byte[] b = File.ReadAllBytes(filePath);
            return b;
        }

        public async Task UploadAsync(string filename, Stream formFile)
        {
            var uploads = Path.Combine(_hostingEnvironment.ContentRootPath, "Uploads");
            if (formFile.Length > 0)
            {
                var filePath = Path.Combine(uploads, filename);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await formFile.CopyToAsync(fileStream);
                }
            }

        }
    }
}
