﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Apolus.MigrationsApp
{
    public class CommandLineOptions
    {
        [Option('m', "migrations", HelpText = "Run database migrations.", Default = false)]
        public bool RunMigrations { get; set; }

        [Option('s', "seed", HelpText = "Seed data into database.", Default = true)]
        public bool SeedData { get; set; }

        [Option('c', "connection-string", HelpText = "Connection string used for selected tasks.")]
        public string ConnectionString { get; set; }
    }
}
