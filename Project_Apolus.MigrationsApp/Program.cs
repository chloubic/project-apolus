﻿using CommandLine;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Project_Apolus.Common.Enums;
using Project_Apolus.DAL;
using Project_Apolus.DAL.Entities;
using Project_Apolus.DAL.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Project_Apolus.MigrationsApp
{
    public class Program
    {
        public static int Main(string[] args)
        {
            var options = new CommandLineOptions();
            var result = Parser.Default
                .ParseArguments<CommandLineOptions>(args)
                .WithParsed(o => options = o);

            if (result is NotParsed<CommandLineOptions>)
            {
                return -1;
            }

            RunWithOptions(options);

            return 0;
        }

        private static void RunWithOptions(CommandLineOptions options)
        {
            var connectionString = options.ConnectionString;
            var appDbContext = CreateDbContext(connectionString);
            if (options.RunMigrations)
            {
                appDbContext.Database.Migrate();
            }

            if (options.SeedData)
            {
                // seed
                SeedDays(appDbContext, connectionString);
                SeedClientOrganization(appDbContext, connectionString);
                SeedRoles(appDbContext, connectionString);
            }
        }

        private static void SeedDays(AppDbContext context, string connectionString)
        {
            // delete all
            context.Set<DayEntity>().RemoveRange(context.Set<DayEntity>());

            var dayNames = new string[]{ "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota", "Neděle" };
            for(var i = 0; i < dayNames.Length; i++)
            {
                var dayEntity = new DayEntity()
                {
                    Id = Guid.NewGuid(),
                    Name = dayNames[i],
                    Order = i
                };
                context.Set<DayEntity>().Add(dayEntity);
            }

            context.SaveChanges();
        }

        private static void SeedRoles(AppDbContext context, string connectionString)
        {
            // Delete all
            context.Roles.RemoveRange(context.Set<AppRole>());

            // Seed new
            var services = new ServiceCollection();
            services.AddDbContext<AppDbContext>(x => x.UseSqlServer(connectionString));
            services.AddIdentityCore<AppUser>(SetupIdentityOptions)
                .AddRoles<AppRole>()
                .AddEntityFrameworkStores<AppDbContext>();
            var serviceProvider = services.BuildServiceProvider();
            var roleManager = serviceProvider.GetRequiredService<RoleManager<AppRole>>();

            var roles = new string[] { Roles.Admin, Roles.Client, Roles.Operator };

            foreach (var role in roles)
            {
                var roleEntity = new AppRole()
                {
                    Name = role,
                    NormalizedName = role.ToLower()
                };
                var result = roleManager.CreateAsync(roleEntity).Result;
            }
        }

        private static void SeedClientOrganization(AppDbContext context, string connectionString)
        {
            // delete all

            context.Set<ReservationEntity>().RemoveRange(context.Set<ReservationEntity>());
            context.Set<TableEntity>().RemoveRange(context.Set<TableEntity>());
            context.Set<RestaurantEntity>().RemoveRange(context.Set<RestaurantEntity>());
            context.Set<AppUserRole>().RemoveRange(context.Set<AppUserRole>());
            context.Set<AppUser>().RemoveRange(context.Set<AppUser>());
            context.Set<OrganizationEntity>().RemoveRange(context.Set<OrganizationEntity>());

            var organizationEntity = new OrganizationEntity()
            {
                Id = Guid.NewGuid(),
                Name = "Clients",
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
                IsClientsOrg = true
            };

            context.Set<OrganizationEntity>().Add(organizationEntity);

            context.SaveChanges();
        }

        private static void SetupIdentityOptions(IdentityOptions options)
        {
            options.User.RequireUniqueEmail = true;
            options.Password.RequireDigit = false;
            options.Password.RequiredLength = 6;
            options.Password.RequireLowercase = false;
            options.Password.RequiredUniqueChars = 0;
            options.Password.RequireNonAlphanumeric = false;
            options.Password.RequireUppercase = false;
        }

        public static AppDbContext CreateDbContext(string connectionString)
        {
            return new AppDbContext(GetDbContextOptions(connectionString));
        }

        private static DbContextOptions<AppDbContext> GetDbContextOptions(string connectionString)
        {
            var builder = new DbContextOptionsBuilder<AppDbContext>();

            builder.UseSqlServer(connectionString, options => options.EnableRetryOnFailure());
            var dbContextOptions = builder.Options;
            return dbContextOptions;
        }
    }
}
